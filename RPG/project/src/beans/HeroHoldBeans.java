package beans;

public class HeroHoldBeans {


	private int id;
	private int hero_id;
	private String name;
	private int recovery;
	private int cost;
	private String icon;
	private int herb_id;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getHero_id() {
		return hero_id;
	}
	public void setHero_id(int hero_id) {
		this.hero_id = hero_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getRecovery() {
		return recovery;
	}
	public void setRecovery(int recovery) {
		this.recovery = recovery;
	}
	public int getCost() {
		return cost;
	}
	public void setCost(int cost) {
		this.cost = cost;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public int getHerb_id() {
		return herb_id;
	}
	public void setHerb_id(int herb_id) {
		this.herb_id = herb_id;
	}


}
