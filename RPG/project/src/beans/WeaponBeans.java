package beans;

public class WeaponBeans {

	private int id;
	private String name;
	private int additional_attack;
	private int cost;
	private String icon;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAdditional_attack() {
		return additional_attack;
	}
	public void setAdditional_attack(int additional_attack) {
		this.additional_attack = additional_attack;
	}
	public int getCost() {
		return cost;
	}
	public void setCost(int cost) {
		this.cost = cost;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}

}
