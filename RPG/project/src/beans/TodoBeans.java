package beans;

public class TodoBeans {

	private int id;
	private String content;
	private int turn;
	private int place_id;
	private String place_icon;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}

	public int getTurn() {
		return turn;
	}
	public void setTurn(int turn) {
		this.turn = turn;
	}
	public int getPlace_id() {
		return place_id;
	}
	public void setPlace_id(int place_id) {
		this.place_id = place_id;
	}
	public String getPlace_icon() {
		return place_icon;
	}
	public void setPlace_icon(String place_icon) {
		this.place_icon = place_icon;
	}

}
