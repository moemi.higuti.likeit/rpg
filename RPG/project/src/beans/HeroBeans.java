package beans;

public class HeroBeans {

	private int id;
	private String name;
	private int max_hp;
	private int hp;
	private int attack;
	private int defense;
	private int lv;
	private int gold;
	private int equip_weapon_id;
	private int getexp;
	private String password;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getMax_hp() {
		return max_hp;
	}
	public void setMax_hp(int max_hp) {
		this.max_hp = max_hp;
	}
	public int getHp() {
		return hp;
	}
	public void setHp(int hp) {
		this.hp = hp;
	}
	public int getAttack() {
		return attack;
	}
	public void setAttack(int attack) {
		this.attack = attack;
	}
	public int getDefense() {
		return defense;
	}
	public void setDefense(int defense) {
		this.defense = defense;
	}
	public int getLv() {
		return lv;
	}
	public void setLv(int lv) {
		this.lv = lv;
	}
	public int getGold() {
		return gold;
	}
	public void setGold(int gold) {
		this.gold = gold;
	}
	public int getEquip_weapon_id() {
		return equip_weapon_id;
	}
	public void setEquip_weapon_id(int equip_weapon_id) {
		this.equip_weapon_id = equip_weapon_id;
	}
	public int getGetexp() {
		return getexp;
	}
	public void setGetexp(int getexp) {
		this.getexp = getexp;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}


}