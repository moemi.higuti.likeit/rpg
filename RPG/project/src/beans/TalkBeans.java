package beans;

public class TalkBeans {

	private int id;
	private int TODO_id;
	private String talk_content;
	private int turn;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getTODO_id() {
		return TODO_id;
	}
	public void setTODO_id(int tODO_id) {
		TODO_id = tODO_id;
	}
	public String getTalk_content() {
		return talk_content;
	}
	public void setTalk_content(String talk_content) {
		this.talk_content = talk_content;
	}
	public int getTurn() {
		return turn;
	}
	public void setTurn(int turn) {
		this.turn = turn;
	}



}
