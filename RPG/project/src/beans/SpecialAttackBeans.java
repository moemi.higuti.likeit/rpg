package beans;

public class SpecialAttackBeans {

	private int id;
	private String attack_name;
	private int hero_lv;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAttack_name() {
		return attack_name;
	}
	public void setAttack_name(String attack_name) {
		this.attack_name = attack_name;
	}
	public int getHero_lv() {
		return hero_lv;
	}
	public void setHero_lv(int hero_lv) {
		this.hero_lv = hero_lv;
	}

}
