package rpgGame;

import java.util.ArrayList;

import beans.HeroBeans;
import beans.MonsterBeans;

public class Attack {


	public static int HeroAttack(int turn, String monsterId, ArrayList<MonsterBeans> monsterList, HeroBeans hero) {


		//ヒーローがモンスターを攻撃するメソッド
		for(int i = 0; i < monsterList.size(); i++) {

			//攻撃対象のモンスターを探す
			if (turn ==  i && monsterList.get(i).getId() == Integer.parseInt(monsterId)) {

				//見つけたらそのモンスターへのダメージ量を計算
				//0.82~それ以上の出るランダム関数を作る
				double attackRan=  (Math.random()*1.6)+0.82;

				//基本のダメージ　を計算
				int baseattack = hero.getAttack() - monsterList.get(i).getDefense();

				//基本ダメージと乱数をかける
				int damageToMonster = (int) (baseattack * attackRan);

				System.out.println();
				System.out.println("モンスターが食らったダメージ"+damageToMonster);

				//上記のダメージ文をそのモンスターのHpを減らす
				monsterList.get(i).setHp(monsterList.get(i).getHp()-damageToMonster);

//				request.setAttribute("HeroAttackeMessage", hero.getName()+" が "+ monsterList.get(i).getName() +turn +" に "+ damageToMonster+"のダメージをあたえた！");

				System.out.println();
				System.out.println("モンスターの残りのHp"+monsterList.get(i).getHp());
				System.out.println();


				return damageToMonster;
				}

			}
		return turn;



	}





}
