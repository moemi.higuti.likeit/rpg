package rpgGame;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.HeroBeans;
import beans.WeaponBeans;
import dao.HeroDao;
import dao.WeaponDao;

/**
 * Servlet implementation class Countinue
 */
@WebServlet("/Countinue")
public class Countinue extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Countinue() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

		//htmlでつっくった画面を出す
		// フォワード　サーブレットからJSPに行く
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/continue.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

		//プレーイヤー名を入手
		String password = request.getParameter("password");

		// 名前がきちんと入っているか調べる
		if (password == null || password.equals("")) {
			request.setAttribute("ErroeMessage","復活の呪文を入れてください");

			//htmlでつっくった画面を出す
			// フォワード　サーブレットからJSPに行く
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/continue.jsp");
			dispatcher.forward(request, response);

			return;
			}

		// ヒーローの情報を入手

		try {
			//パスワードからヒーローの情報を取得
			HeroBeans hero = HeroDao.getHeroInfoByPassword(password);

			if (hero == null) {
				// リクエストスコープにエラーメッセージをセット
				request.setAttribute("ErroeMessage", "失敗しました");

				//htmlでつっくった画面を出す
				// フォワード　サーブレットからJSPに行く
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/continue.jsp");
				dispatcher.forward(request, response);
				return;
			}


	/*--------------------------------------------------------------------------------------------------------*/

			WeaponBeans heroitem = WeaponDao.getWeaponById(hero.getEquip_weapon_id());

			if( heroitem.getId() != 0) {

				//ヒーローが持っているアイテム(あらかじめデータベースにあった情報をもとに作る)の情報をセッションで取得
				ArrayList<WeaponBeans> HeroHoldWeaponList = new ArrayList<WeaponBeans>();
				 HeroHoldWeaponList.add(heroitem);

				System.out.println("ーーーーアイテムとってこれたーーーー");
				System.out.println();

				//リクエストパラメーターにセット
				session.setAttribute("HeroHoldWeaponList", HeroHoldWeaponList);
			}

			session.setAttribute("hero", hero);

			//上で取得した情報をもってjspに行く
			response.sendRedirect("Town");


		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}


	}

}
