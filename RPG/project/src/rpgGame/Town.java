package rpgGame;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.HeroHoldBeans;
import beans.TodoBeans;
import beans.WeaponBeans;
import dao.TodoDao;

/**
 * Servlet implementation class Town
 */
@WebServlet("/Town")
public class Town extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Town() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

		//ヒーローが持っているアイテム(あらかじめデータベースにあった情報をもとに作る)の情報をセッションで取得
		ArrayList<HeroHoldBeans> HeroHoldList = (ArrayList<HeroHoldBeans>) session.getAttribute("HeroHoldList");

		if (HeroHoldList == null) {
			HeroHoldList = new ArrayList<HeroHoldBeans>();
			session.setAttribute("HeroHoldList", HeroHoldList);
		}

		//ヒーローが持っているアイテム(あらかじめデータベースにあった情報をもとに作る)の情報をセッションで取得
		ArrayList<WeaponBeans> HeroHoldWeaponList = (ArrayList<WeaponBeans>) session.getAttribute("HeroHoldWeaponList");

		if (HeroHoldWeaponList == null) {
			HeroHoldWeaponList = new ArrayList<WeaponBeans>();
			session.setAttribute("HeroHoldWeaponList", HeroHoldWeaponList);
		}

		try {


			// 画面街中での外に出るコマンド情報を入手
			ArrayList<TodoBeans> ToDoListOut = TodoDao.ToDoListOut();

			// 画面　街中での町の中の散策コマンド情報を入手
			ArrayList<TodoBeans> ToDoListInTown = TodoDao.ToDoListInTown();

			session.setAttribute("ToDoListOut", ToDoListOut);
			session.setAttribute("ToDoListInTown", ToDoListInTown);

			//htmlでつっくった画面を出す
			// フォワード　サーブレットからJSPに行く
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/town02.jsp");
			dispatcher.forward(request, response);

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}


/*
 * //hrtoのインスタンスを作成
			HeroBeans hero = new HeroBeans(heroInfo.getId(),heroInfo.getName(),heroInfo.getMax_hp()
					,heroInfo.getHp(),heroInfo.getAttack(),heroInfo.getDefense(),heroInfo.getLv(),heroInfo.getGold(),
					heroInfo.getEquip_weapon_id()
					,heroInfo.getHold_item_id1(),heroInfo.getHold_item_id2(),heroInfo.getHold_item_id3(),
					heroInfo.getHold_item_id4(),heroInfo.getHold_item_id5(),heroInfo.getHold_item_id6(),
					heroInfo.getHold_item_id7(),heroInfo.getHold_item_id8(),heroInfo.getHold_item_id9(),
					heroInfo.getHold_item_id10());


								//hrtoのインスタンスを作成
			HeroBeans hero = new HeroBeans(heroInfo.getId(), heroInfo.getName(), heroInfo.getMax_hp()
					, heroInfo.getHp(), heroInfo.getAttack(), heroInfo.getDefense(), heroInfo.getLv(), heroInfo.getGold());


 *
 * */
