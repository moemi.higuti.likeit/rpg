package rpgGame;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.HeroBeans;
import beans.WeaponBeans;

/**
 * Servlet implementation class WeaponShopSell
 */
@WebServlet("/WeaponShopSell")
public class WeaponShopSell extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public WeaponShopSell() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

/*--------------------------------------------------------------------------------------------------------*/

		//オープニングで作ったヒーローインスタンスを取得
		HeroBeans hero =  (HeroBeans) session.getAttribute("hero");
		//もしヒーローインスタンスがとれなかったらオープニング画面に戻る
		if (hero == null) {
			response.sendRedirect("Opening");
			return;
		}

		//JSPかたメッセージを受け取る
		String ItemMessage =request.getParameter("ItemMessage");

/*--------------------------------------------------------------------------------------------------------*/

		//ヒーローが持っているアイテム(あらかじめデータベースにあった情報をもとに作る)の情報をセッションで取得
		ArrayList<WeaponBeans> HeroHoldWeaponList = (ArrayList<WeaponBeans>) session.getAttribute("HeroHoldWeaponList");

		if (HeroHoldWeaponList == null) {
			HeroHoldWeaponList = new ArrayList<WeaponBeans>();
			session.setAttribute("HeroHoldWeaponList", HeroHoldWeaponList);
		}


/*--------------------------------------------------------------------------------------------------------*/

		//ヒーローの持っているアイテムの配列の順番を取得
		int turnHoldItem = Integer.parseInt(request.getParameter("i"));
		// 武器の値段を持ってくる
		int SellMoney =Integer.parseInt(request.getParameter("Cost"));
		// 攻撃対象のモンスターのIdを持ってくる
		String sellItemId = request.getParameter("holdId");


		//取得した売る金額を使い　ヒーローのゴールドを増やす
		hero.setGold(hero.getGold()+SellMoney);
		System.out.print(hero.getGold());

		//上で取得した配列の順番をもとにその配列を消す
		HeroHoldWeaponList.remove(HeroHoldWeaponList.get(turnHoldItem));

		System.out.println("ーーーーアイテム売れた！ーーーー");
		System.out.println();

		request.setAttribute("ItemMessage", "買取金額 " + SellMoney+ "G です。　ありがとうございます！！！！！");

		if(HeroHoldWeaponList==null || HeroHoldWeaponList.size()==0) {

			session.removeAttribute("HeroHoldList");

		}else {
			session.setAttribute("HeroHoldList", HeroHoldWeaponList);
		}

		session.setAttribute("hero", hero);

		//htmlでつっくった画面を出す
		// フォワード　サーブレットからJSPに行く
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/weaponshop.jsp");
		dispatcher.forward(request, response);


	}

}
