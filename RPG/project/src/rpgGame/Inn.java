package rpgGame;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.HeroBeans;

/**
 * Servlet implementation class Inn
 */
@WebServlet("/Inn")
public class Inn extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Inn() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

/*--------------------------------------------------------------------------------------------------------*/

		//オープニングで作ったヒーローインスタンスを取得
		HeroBeans hero =  (HeroBeans) session.getAttribute("hero");

		//もしヒーローインスタンスがとれなかったらオープニング画面に戻る
		if (hero == null) {
			response.sendRedirect("Opening");
			return;
		}

		if(hero.getGold()<100) {
			System.out.println("ーーーーゴールドが足りませんーーーー");
			System.out.println();
			request.setAttribute("RestMessage", "ゴールドが足りません。。。");

			//htmlでつっくった画面を出す
			// フォワード　サーブレットからJSPに行く
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/inn.jsp");
			dispatcher.forward(request, response);
			return;
		}

		hero.setGold(hero.getGold()-100);
		hero.setHp(hero.getMax_hp());

		System.out.println("ーーーー体力回復ーーーー");
		System.out.println();
		request.setAttribute("RestMessage", "おはようございます。　 今日も頑張ってください!!!!");

		//htmlでつっくった画面を出す
		// フォワード　サーブレットからJSPに行く
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/inn.jsp");
		dispatcher.forward(request, response);
	}

}
