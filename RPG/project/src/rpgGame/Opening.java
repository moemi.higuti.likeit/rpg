package rpgGame;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Opening
 */
@WebServlet("/Opening")
public class Opening extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Opening() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

//		// ヒーローの情報を入手
//		HeroBeans hero;
//		try {
//			hero = HeroDao.getHeroInfo();
//
//			//リクエストパラメーターにセット
//			session.setAttribute("hero", hero);
//		} catch (SQLException e) {
//			// TODO 自動生成された catch ブロック
//			e.printStackTrace();
//		}




			//ヒーローが持っている薬草のアイテム情報をすべて取得する
			//ヒーローが持っている道具のリストを作る
//			ArrayList<HeroHoldBeans> HeroHoldList = new ArrayList<HeroHoldBeans>();
//			for(int i = 0; i<9 ; i++) {
//
//				int num = 1+i;
//				HeroHoldBeans heroitem = HeroHoldDao.getItemById(num);
//
//				if(heroitem.getName()!=null) {
//
//					HeroHoldDao.ImsertItem(heroitem, hero.getId());
//					HeroHoldList.add(heroitem);
//				}
//
//			}

//			session.setAttribute("HeroHoldList", HeroHoldList);

			//htmlでつっくった画面を出す
			// フォワード　サーブレットからJSPに行く
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/opening.jsp");
			dispatcher.forward(request, response);
			return;



	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
