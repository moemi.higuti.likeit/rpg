package rpgGame;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.HeroBeans;
import beans.MonsterBeans;
import dao.MonsterDao;

/**
 * Servlet implementation class RunAway
 */
@WebServlet("/RunAway")
public class RunAway extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public RunAway() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

/*--------------------------------------------------------------------------------------------------------*/

		//オープニングで作ったヒーローインスタンスを取得
		HeroBeans hero =  (HeroBeans) session.getAttribute("hero");
		//もしヒーローインスタンスがとれなかったらオープニング画面に戻る
		if (hero == null) {
			response.sendRedirect("Opening");
			return;
		}

/*--------------------------------------------------------------------------------------------------------*/

		//出てくるモンスターの情報をを取得
		ArrayList<MonsterBeans> monsterList = (ArrayList<MonsterBeans>) session.getAttribute("monsterList");

/*--------------------------------------------------------------------------------------------------------*/

		int runAwayRan= (int) ((Math.random()*1.6)+0.82);

		//コマンド逃げるが成功する確率を求める　レベルが上がるにつれ成功しやすく　モンスターの数が多いほど失敗する
		Boolean Success  =
				0.8 <  hero.getLv() / monsterList.size() * runAwayRan;

		//レベル１　敵１なら85がでる

		//もし失敗なら
		if (!Success) {

			request.setAttribute("HeroAttackeMessage", "囲まれた!　逃げられない!!!");
			//htmlでつっくった画面を出す
			// フォワード　サーブレットからJSPに行く
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/battle.jsp");
			dispatcher.forward(request, response);
			return;

		}else {

			//倒したモンスターリスト情報をセッションで取得
			ArrayList<MonsterBeans> DefeatMonsterList = (ArrayList<MonsterBeans>) session.getAttribute("DefeatMonsterList");

			//セッションに上の倒したモンスターがない場合倒したモンスターリストを作成
			if (DefeatMonsterList == null) {

				DefeatMonsterList = new ArrayList<MonsterBeans>();

			}


			request.setAttribute("AnnihilationMessage", "うまく逃げられた！！");

			int totalGold = MonsterDao.getTotalMonsterGold(DefeatMonsterList);
			int totalExp = MonsterDao.getTotalMonsterExp(DefeatMonsterList);

			hero.setGold(hero.getGold()+totalGold);
			hero.setGetexp(hero.getGetexp()+totalExp);

			request.setAttribute("MonsterAttackeMessage3",hero.getName()+"は"+ totalGold +"G　を手に入れた！");

			request.setAttribute("MonsterAttackeMessage2",hero.getName()+"は"+ totalExp +"の経験値を手に入れた！");

			//倒したモンスターのリストのセッションを切る
			session.removeAttribute("DefeatMonsterList");

			//htmlでつっくった画面を出す
			// フォワード　サーブレットからJSPに行く
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/battle.jsp");
			dispatcher.forward(request, response);
			return;
		}

	}

}
