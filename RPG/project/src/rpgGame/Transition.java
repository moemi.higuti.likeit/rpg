package rpgGame;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.HerbBeans;
import beans.HeroBeans;
import beans.MonsterBeans;
import beans.SpecialAttackBeans;
import beans.TodoDetailBeans;
import beans.WeaponBeans;
import dao.HerbDao;
import dao.MonsterDao;
import dao.SpecialAttackDao;
import dao.TodoDetailDao;
import dao.WeaponDao;

/**
 * Servlet implementation class Transition
 */
@WebServlet("/Transition")
public class Transition extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Transition() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

    	/*--------------------------------------------------------------------------------------------------------*/

		//オープニングで作ったヒーローインスタンスを取得
		HeroBeans hero =  (HeroBeans) session.getAttribute("hero");
		//もしヒーローインスタンスがとれなかったらオープニング画面に戻る
		if (hero == null) {
			response.sendRedirect("Opening");
			return;
		}

/*--------------------------------------------------------------------------------------------------------*/

		//URLからtodo.id　の情報を取得
		String todoId = request.getParameter("todoId");

		try {

			ArrayList<TodoDetailBeans> todoDetailList = TodoDetailDao.getComandAtBattle(todoId);

			//モンスターリスト情報を入れるインスタンスを作る
			ArrayList<MonsterBeans> monsterList =  new ArrayList<MonsterBeans>();

			//1~5の数が出るランダム関数を作る

			int m =  (int)(Math.random()*5)+1;

			if(todoId.equals("2")) {
				m=1;
			}

		    for(int i=0 ; i<m ; i++) {

		    	//対象のアイテム情報を取得
				MonsterBeans monster = MonsterDao.getMonsterSingle(todoId);

		    	//カートに商品を追加。
		    	monsterList.add(monster);


		    }

/*--------------------------------------------------------------------------------------------------------*/

	    	//ヒーローの特技をリストで取得
			ArrayList<SpecialAttackBeans> SpecialAttackList = SpecialAttackDao.getSpecialAttackList(hero.getLv());

			//セッションで持たせる
			session.setAttribute("SpecialAttackList", SpecialAttackList);

			switch (todoId) {

			case "1":


			    session.setAttribute("m", m);
			    session.setAttribute("background", "mountain");
			    session.setAttribute("monsterList", monsterList);
			    session.setAttribute("todoDetailList", todoDetailList);
			    request.setAttribute("AttackeMessage", "モンスターが "+ m +" 匹 あらわれた");

				//htmlでつっくった画面を出す
				// フォワード　サーブレットからJSPに行く
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/battle.jsp");
				dispatcher.forward(request, response);
				break;

			case "2":

				//htmlでつっくった画面を出す
				// フォワード　サーブレットからJSPに行く
				session.setAttribute("background", "mountain");
			    session.setAttribute("monsterList", monsterList);
			    session.setAttribute("todoDetailList", todoDetailList);
			    request.setAttribute("AttackeMessage", "魔王が あらわれた");
				session.setAttribute("background", "kingdom");

				RequestDispatcher dispatcher2 = request.getRequestDispatcher("/WEB-INF/jsp/battle.jsp");
				dispatcher2.forward(request, response);
				break;

			case "3":

				//薬草のアイテム情報をすべて取得する
				ArrayList<HerbBeans> itemShopList = HerbDao.getItemAll();

				//上記の道具やで売っているリストをセットする
			    session.setAttribute("itemShopList", itemShopList);

			  //htmlでつっくった画面を出す
				// フォワード　サーブレットからJSPに行く
				RequestDispatcher dispatcher3 = request.getRequestDispatcher("/WEB-INF/jsp/itemshop.jsp");
				dispatcher3.forward(request, response);
				break;

			case "4":

				//htmlでつっくった画面を出す
				// フォワード　サーブレットからJSPに行く
				RequestDispatcher dispatcher4 = request.getRequestDispatcher("/WEB-INF/jsp/inn.jsp");
				dispatcher4.forward(request, response);
				break;

			case "5":

			//薬草のアイテム情報をすべて取得する
			ArrayList<WeaponBeans> WeaponShopList = WeaponDao.getWeaponAll();

			//上記の道具やで売っているリストをセットする
		    session.setAttribute("WeaponShopList", WeaponShopList);

		  //htmlでつっくった画面を出す
			// フォワード　サーブレットからJSPに行く
			RequestDispatcher dispatcher5 = request.getRequestDispatcher("/WEB-INF/jsp/weaponshop.jsp");
			dispatcher5.forward(request, response);
			break;

			case "6": //カジノ

				//htmlでつっくった画面を出す
				// フォワード　サーブレットからJSPに行く
				RequestDispatcher dispatcher6 = request.getRequestDispatcher("/WEB-INF/jsp/casino.jsp");
				dispatcher6.forward(request, response);
				break;


			case "7"://教会

				//上記の道具やで売っているリストをセットする
			    session.setAttribute("FatherMessage", "生きとし生けるものはみな　神の子。わが教会に どんな　ご用かな？");

				//htmlでつっくった画面を出す
				// フォワード　サーブレットからJSPに行く
				RequestDispatcher dispatcher7 = request.getRequestDispatcher("/WEB-INF/jsp/church02.jsp");
				dispatcher7.forward(request, response);
				break;


			case "8"://森に行く

//				//モンスターリスト情報を入れるインスタンスを作る
//				ArrayList<MonsterBeans> monsterList =  new ArrayList<MonsterBeans>();
//
//				//1~5の数が出るランダム関数を作る
//				int m =  (int)(Math.random()*5)+1;
//
//			    for(int i=0 ; i<m ; i++) {
//
//			    	//対象のアイテム情報を取得
//					MonsterBeans monster = MonsterDao.getMonsterSingle(todoId);
//
//			    	//カートに商品を追加。
//			    	monsterList.add(monster);
//
//			    }

			    session.setAttribute("m", m);
			    session.setAttribute("background", "forest01");
			    session.setAttribute("monsterList", monsterList);
			    session.setAttribute("todoDetailList", todoDetailList);
			    request.setAttribute("AttackeMessage", "モンスターが "+ m +" 匹 あらわれた");

				//htmlでつっくった画面を出す
				// フォワード　サーブレットからJSPに行く
				RequestDispatcher dispatcher8 = request.getRequestDispatcher("/WEB-INF/jsp/battle.jsp");
				dispatcher8.forward(request, response);
				break;



			case "9"://まのもりにいく

//				//モンスターリスト情報を入れるインスタンスを作る
//				ArrayList<MonsterBeans> monsterList =  new ArrayList<MonsterBeans>();
//
//				//1~5の数が出るランダム関数を作る
//				int m =  (int)(Math.random()*5)+1;
//
//			    for(int i=0 ; i<m ; i++) {
//
//			    	//対象のアイテム情報を取得
//					MonsterBeans monster = MonsterDao.getMonsterSingle(todoId);
//
//			    	//カートに商品を追加。
//			    	monsterList.add(monster);
//
//			    }

			    session.setAttribute("m", m);
			    session.setAttribute("background", "forest");
			    session.setAttribute("monsterList", monsterList);
			    session.setAttribute("todoDetailList", todoDetailList);
			    request.setAttribute("AttackeMessage", "モンスターが "+ m +" 匹 あらわれた");

				//htmlでつっくった画面を出す
				// フォワード　サーブレットからJSPに行く
				RequestDispatcher dispatcher9 = request.getRequestDispatcher("/WEB-INF/jsp/battle.jsp");
				dispatcher9.forward(request, response);
				break;



			}


		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
