package rpgGame;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.HeroBeans;
import beans.HeroHoldBeans;
import beans.TodoBeans;

/**
 * Servlet implementation class UseItem
 */
@WebServlet("/UseItem")
public class UseItem extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UseItem() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

/*--------------------------------------------------------------------------------------------------------*/

		//オープニングで作ったヒーローインスタンスを取得
		HeroBeans hero =  (HeroBeans) session.getAttribute("hero");
		//もしヒーローインスタンスがとれなかったらオープニング画面に戻る
		if (hero == null) {
			response.sendRedirect("Opening");
			return;
		}

		//JSPからメッセージを受け取る
		String ItemMessage =request.getParameter("ItemMessage");

		//JSPから次に行くところのjsp名を受け取る
		String jsp =request.getParameter("jsp");

/*--------------------------------------------------------------------------------------------------------*/

		//ヒーローが持っているアイテム(あらかじめデータベースにあった情報をもとに作る)の情報をセッションで取得
		ArrayList<HeroHoldBeans> HeroHoldList = (ArrayList<HeroHoldBeans>) session.getAttribute("HeroHoldList");

		if (HeroHoldList == null) {
			HeroHoldList = new ArrayList<HeroHoldBeans>();
			session.setAttribute("HeroHoldList", HeroHoldList);
		}

/*--------------------------------------------------------------------------------------------------------*/

		//ヒーローの持っているアイテムの配列の順番を取得
		int turnHoldItem = Integer.parseInt(request.getParameter("i"));
		// 攻撃対象のモンスターのIdを持ってくる
		int SellMoney =Integer.parseInt(request.getParameter("Cost"));
		// 攻撃対象のモンスターのIdを持ってくる
		int Recovery = Integer.parseInt(request.getParameter("Recovery"));


		//取得した売る金額を使い　ヒーローのゴールドを増やす
		hero.setGold(hero.getGold()+SellMoney);
		System.out.print(hero.getGold());

		//上で取得した配列の順番をもとにその配列を消す
		HeroHoldList.remove(HeroHoldList.get(turnHoldItem));

		System.out.println("ーーーーアイテム使えた！ーーーー");
		System.out.println();

		int recoveryHp = hero.getHp()+Recovery;
		if(recoveryHp > hero.getMax_hp()) {
			recoveryHp = hero.getMax_hp();
		}
		hero.setHp(recoveryHp);

		request.setAttribute("HeroAttackeMessage", hero.getName()+"は " + Recovery+ "ポイント　回復した！！！");

		if(HeroHoldList==null || HeroHoldList.size()==0) {

			session.removeAttribute("HeroHoldList");

		}else {
			session.setAttribute("HeroHoldList", HeroHoldList);
		}

		session.setAttribute("hero", hero);
		// 画面街中での外に出るコマンド情報を入手
		ArrayList<TodoBeans> ToDoListOut = (ArrayList<TodoBeans>) session.getAttribute("ToDoListOut");
		ArrayList<TodoBeans>  ToDoListInTown =  (ArrayList<TodoBeans>) session.getAttribute("ToDoListInTown");

		session.setAttribute("ToDoListOut", ToDoListOut);
		session.setAttribute("ToDoListInTown", ToDoListInTown);

		//htmlでつっくった画面を出す
		// フォワード　サーブレットからJSPに行く
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/"+jsp+".jsp");
		dispatcher.forward(request, response);


	}

}
