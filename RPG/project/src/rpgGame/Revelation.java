package rpgGame;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.HeroBeans;
import beans.StatusBeans;
import dao.StatusDao;

/**
 * Servlet implementation class Revelation
 */
@WebServlet("/Revelation")
public class Revelation extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Revelation() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

		//オープニングで作ったヒーローインスタンスを取得
		HeroBeans hero = (HeroBeans) session.getAttribute("hero");

		//ステータス情報を変数に入れる
		ArrayList<StatusBeans> StatusList;
		try {
			StatusList = StatusDao.getStatusInfo();

			int nextLv = StatusList.get(hero.getLv() - 1).getNeedExp() - hero.getGetexp();

			System.out.println(nextLv);

			int restExp = (int) (hero.getGetexp() - StatusList.get(hero.getLv() - 1).getNeedExp());

			request.setAttribute("ErroeMessage",
					"神の声が聞こえます。" + hero.getName() + "よ. あと" + nextLv + "の経験で次のレベルになるでしょう。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/church02.jsp");
			dispatcher.forward(request, response);

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

	}

}
