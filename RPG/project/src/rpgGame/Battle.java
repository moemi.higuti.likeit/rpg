package rpgGame;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.HeroBeans;
import beans.HeroHoldBeans;
import beans.MonsterBeans;
import beans.StatusBeans;
import beans.WeaponBeans;
import dao.MonsterDao;
import dao.StatusDao;

/**
 * Servlet implementation class Battle
 */
@WebServlet("/Battle")
public class Battle extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Object ToDoDetailList = null;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Battle() {
        super();
        // TODO Auto-generated constructor stub
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    	HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

    	/*--------------------------------------------------------------------------------------------------------*/

		//オープニングで作ったヒーローインスタンスを取得
		HeroBeans hero =  (HeroBeans) session.getAttribute("hero");
		//もしヒーローインスタンスがとれなかったらオープニング画面に戻る
		if (hero == null) {
			response.sendRedirect("Opening");
			return;
		}

/*--------------------------------------------------------------------------------------------------------*/

    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

/*--------------------------------------------------------------------------------------------------------*/

		//オープニングで作ったヒーローインスタンスを取得
		HeroBeans hero =  (HeroBeans) session.getAttribute("hero");
		//もしヒーローインスタンスがとれなかったらオープニング画面に戻る
		if (hero == null) {
			response.sendRedirect("Opening");
			return;
		}

		//戦闘が終了したメッセージがあったら持ってくる
		String background = request.getParameter("background");
		session.setAttribute("background", background);

/*--------------------------------------------------------------------------------------------------------*/

		//戦闘が終了したメッセージがあったら持ってくる
		String AnnihilationMessage = request.getParameter("AnnihilationMessage");
		//そのメッセージがあったら町に戻る
		if(AnnihilationMessage != "") {

			if(background.equals("kingdom")) {

				//htmlでつっくった画面を出す
				// フォワード　サーブレットからJSPに行く
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/gameclear.jsp");
				dispatcher.forward(request, response);
				return;

			}

			//ヒーローのセリフ
			request.setAttribute("HeroAttackeMessage", "たたかいはたいへんだ");
			//htmlでつっくった画面を出す
			// フォワード　サーブレットからJSPに行く
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/town02.jsp");
			dispatcher.forward(request, response);
			return;
		}



/*--------------------------------------------------------------------------------------------------------*/


		//倒したモンスターリスト情報をセッションで取得
		ArrayList<MonsterBeans> DefeatMonsterList = (ArrayList<MonsterBeans>) session.getAttribute("DefeatMonsterList");

		//セッションに上の倒したモンスターがない場合倒したモンスターリストを作成
		if (DefeatMonsterList == null) {

			DefeatMonsterList = new ArrayList<MonsterBeans>();
			session.setAttribute("DefeatMonsterList", DefeatMonsterList);

		}


/*--------------------------------------------------------------------------------------------------------*/

		//ヒーローがやられたときの処理
		String GameOverMessage = request.getParameter("GameOverMessage");
		//そのメッセージがあったら町に戻る
		if(GameOverMessage != null && GameOverMessage != "") {
			//htmlでつっくった画面を出す
			// フォワード　サーブレットからJSPに行く
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/gameover.jsp");
			dispatcher.forward(request, response);
		}

		//出てくるモンスターの情報をを取得
		ArrayList<MonsterBeans> monsterList = (ArrayList<MonsterBeans>) session.getAttribute("monsterList");

		//ヒーローが持っているアイテム(あらかじめデータベースにあった情報をもとに作る)の情報をセッションで取得
		ArrayList<HeroHoldBeans> HeroHoldList = (ArrayList<HeroHoldBeans>) session.getAttribute("HeroHoldList");


		String HeroAttackeMessage = request.getParameter("HeroAttackeMessage");
		//ヒーローが持っているアイテム(あらかじめデータベースにあった情報をもとに作る)の情報をセッションで取得
		ArrayList<WeaponBeans> HeroHoldWeaponList = (ArrayList<WeaponBeans>) session.getAttribute("HeroHoldWeaponList");


/*--------------------------------------------------------------------------------------------------------*/

		//ヒーローが攻撃したかどうか確かめる
		@SuppressWarnings("unused")
		//ヒーローがアッタクしたときはHeroAttackeMessageのメッセージをJspから持ってこれるのでそれで判断
		Boolean isHeroAttacked = HeroAttackeMessage != ""
				? HeroAttackeMessage != ""
				:false;

		//もしヒーローがまだできていないようなら攻撃させる
		if (!isHeroAttacked) {


			//上で取得した武器情報を通常攻撃に加算していく
			@SuppressWarnings("unused")
			int AdditionalAttack=0;

			if(HeroHoldWeaponList != null) {

				for(int i =0; i<HeroHoldWeaponList.size(); i++) {

					//一応持っている武器のアディショナル攻撃を加算していく
					AdditionalAttack += HeroHoldWeaponList.get(i).getAdditional_attack();

				}

			}

			//モンスターの配列の順番を取得
			int turn = Integer.parseInt(request.getParameter("i"));
			// 攻撃対象のモンスターのIdを持ってくる
			String monsterId = request.getParameter("monsterId");

			//ヒーローがモンスターを攻撃するメソッド
			for(int i = 0; i < monsterList.size(); i++) {

				//攻撃対象のモンスターを探す
				if (turn ==  i && monsterList.get(i).getId() == Integer.parseInt(monsterId)) {

					//見つけたらそのモンスターへのダメージ量を計算
					//0.82~それ以上の出るランダム関数を作る
					double attackRan=  (Math.random()*1.6)+0.82;

					//基本のダメージ　を計算
					int baseattack = hero.getAttack() - monsterList.get(i).getDefense()+AdditionalAttack;

					System.out.println("余分攻撃は"+AdditionalAttack);

					//基本ダメージと乱数をかける
					int damageToMonster = (int) (baseattack * attackRan);

					if(damageToMonster<=0) {
						damageToMonster = 1;
					}

					System.out.println();
					System.out.println("モンスターが食らったダメージ"+damageToMonster);

					//上記のダメージ文をそのモンスターのHpを減らす
					monsterList.get(i).setHp(monsterList.get(i).getHp()-damageToMonster);

					request.setAttribute("HeroAttackeMessage", hero.getName()+" が "+ monsterList.get(i).getName() +turn +" に "+ damageToMonster+"のダメージをあたえた！");

					System.out.println();
					System.out.println("モンスターの残りのHp"+monsterList.get(i).getHp());
					System.out.println();

					//もしモンスターのHPが0になったらの処理をする
					if(monsterList.get(i).getHp()<=0) {

						request.setAttribute("HeroAttackeMessage",damageToMonster+"のダメージをあたえた！" +   hero.getName()+" が "+ monsterList.get(i).getName() +turn +"をやっつけた！");

						//倒したモンスターを倒したリストに入れる
						DefeatMonsterList.add(monsterList.get(turn));

						//倒したモンスターを生存モンスターリストから外す
						monsterList.remove(monsterList.get(turn));
						System.out.println("敵をやっつけた");
						System.out.println();
						session.setAttribute("DefeatMonsterList", DefeatMonsterList);
						session.setAttribute("HeroHoldList", HeroHoldList);

						}

/*------------------------------------------モンスターを全員倒した場合--------------------------------------------*/

					//モンスター全部を倒した場合
					if(monsterList.size() == 0) {

						request.setAttribute("AnnihilationMessage","全ての敵を倒した");

/*------------------------------------------倒したモンスターの情報をゲットしていく--------------------------------------------*/

						int totalGold = MonsterDao.getTotalMonsterGold(DefeatMonsterList);
						int totalExp = MonsterDao.getTotalMonsterExp(DefeatMonsterList);

						hero.setGold(hero.getGold()+totalGold);
						hero.setGetexp(hero.getGetexp()+totalExp);

						request.setAttribute("MonsterAttackeMessage3",hero.getName()+"は"+ totalGold +"G　を手に入れた！");

						request.setAttribute("MonsterAttackeMessage2",hero.getName()+"は"+ totalExp +"の経験値を手に入れた！");

						//倒したモンスターのリストのセッションを切る
						session.removeAttribute("DefeatMonsterList");


						//レベルUPの処理 ステータス情報を取得

						try {
							//ステータス情報を変数に入れる
							ArrayList<StatusBeans> StatusList = StatusDao.getStatusInfo();

							if(hero.getGetexp( ) >= StatusList.get(hero.getLv()-1).getNeedExp()){

							System.out.println("レベルアップおめでとう！");

							int restExp =(int) (hero.getGetexp( ) - StatusList.get(hero.getLv()-1).getNeedExp());

							// レベルアップしたのでステータスを更新
							hero.setGetexp(restExp);
							hero.setLv(hero.getLv()+1);
							hero.setAttack(StatusList.get(hero.getLv()-1).getAttack());
							hero.setMax_hp(StatusList.get(hero.getLv()-1).getMax_hp());
							hero.setDefense(StatusList.get(hero.getLv()-1).getDefense());

							session.setAttribute("hero", hero);
							session.setAttribute("background", background);
							session.setAttribute("HeroHoldList", HeroHoldList);
							//もしもデータベースの情報を変えたいのならここで変えられる
							request.setAttribute("MonsterAttackeMessage1",hero.getName()+"はレベルアップした！");

					}

						} catch (SQLException e) {
							// TODO 自動生成された catch ブロック
							e.printStackTrace();
						}

					}

						//htmlでつっくった画面を出す
						// フォワード　サーブレットからJSPに行く
						RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/battle.jsp");
						dispatcher.forward(request, response);

						return;

					}

				}

		}


/*--------------------------------------------------------------------------------------------------------*/


		else {
		//モンスターがヒーローを攻撃
		//ヒーローがモンスターを攻撃するメソッド
		if(hero.getHp()>=0) {

			for(int i = 0; i < monsterList.size(); i++) {

				//見つけたらそのモンスターへのダメージ量を計算
				//0.82~それ以上の出るランダム関数を作る
				double attackRan=  (Math.random()*1)+0.82;

				//基本のダメージ　を計算
				int baseattack = monsterList.get(i).getAttack() - hero.getDefense();

				//基本ダメージと乱数をかける
				int damageToHero = (int) (baseattack * attackRan);

				System.out.println("before : ヒーローが食らったダメージ"+damageToHero);

				if(damageToHero<=0) {
					damageToHero = 5;
				}

				System.out.println("ヒーローが食らったダメージ"+damageToHero);

				//上記のダメージ文をそのモンスターのHpを減らす
				hero.setHp(hero.getHp()-damageToHero);;

				request.setAttribute("MonsterAttackeMessage"+i, hero.getName()+" は "+ monsterList.get(i).getName()  +"から  "+ damageToHero+"のダメージをくらった！");

				System.out.println("ヒーローの残りのHp"+hero.getHp());
				System.out.println();

				if(hero.getHp() <= 0) {

					request.setAttribute("GameOverMessage","全滅した。。。");

					System.out.println("全滅した。。。");
					System.out.println();
					session.setAttribute("background", background);
					session.setAttribute("HeroHoldList", HeroHoldList);

					//htmlでつっくった画面を出す
					// フォワード　サーブレットからJSPに行く
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/battle.jsp");
					dispatcher.forward(request, response);

					return;
					}
				}


			//htmlでつっくった画面を出す
			// フォワード　サーブレットからJSPに行く
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/battle.jsp");
			dispatcher.forward(request, response);
			return;

		//もしヒーローのHPが0になったらの処理をする
		}

/*--------------------------------------------------------------------------------------------------------*/


		//htmlでつっくった画面を出す
		// フォワード　サーブレットからJSPに行く
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/battle.jsp");
		dispatcher.forward(request, response);


	}
	}}



/*
 * //もしヒーローがまだできていないようなら攻撃させる
		if (!isHeroAttacked) {

			//モンスターの配列の順番を取得
			int turn = Integer.parseInt(request.getParameter("i"));
			// 攻撃対象のモンスターのIdを持ってくる
			String monsterId = request.getParameter("monsterId");

			//ヒーローがモンスターを攻撃するメソッド
			for(int i = 0; i < monsterList.size(); i++) {

				//攻撃対象のモンスターを探す
				if (turn ==  i && monsterList.get(i).getId() == Integer.parseInt(monsterId)) {

					//見つけたらそのモンスターへのダメージ量を計算
					//0.82~それ以上の出るランダム関数を作る
					double attackRan=  (Math.random()*1.6)+0.82;

					//基本のダメージ　を計算
					int baseattack = hero.getAttack() - monsterList.get(i).getDefense();

					//基本ダメージと乱数をかける
					int damageToMonster = (int) (baseattack * attackRan);

					System.out.println();
					System.out.println("モンスターが食らったダメージ"+damageToMonster);

					//上記のダメージ文をそのモンスターのHpを減らす
					monsterList.get(i).setHp(monsterList.get(i).getHp()-damageToMonster);

					request.setAttribute("HeroAttackeMessage", hero.getName()+" が "+ monsterList.get(i).getName() +turn +" に "+ damageToMonster+"のダメージをあたえた！");

					System.out.println();
					System.out.println("モンスターの残りのHp"+monsterList.get(i).getHp());
					System.out.println();

					//もしモンスターのHPが0になったらの処理をする
					if(monsterList.get(i).getHp()<=0) {

						request.setAttribute("HeroAttackeMessage",damageToMonster+"のダメージをあたえた！" +   hero.getName()+" が "+ monsterList.get(i).getName() +turn +"をやっつけた！");
						monsterList.remove(monsterList.get(turn));
						System.out.println("敵をやっつけた");
						System.out.println();

						}
					}
				}

			//モンスター全部を倒した場合
			if(monsterList.size() == 0) {

				request.setAttribute("AnnihilationMessage","敵が全滅した");

				//htmlでつっくった画面を出す
				// フォワード　サーブレットからJSPに行く
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/battle.jsp");
				dispatcher.forward(request, response);

			}else {
			//htmlでつっくった画面を出す
			// フォワード　サーブレットからJSPに行く
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/battle.jsp");
			dispatcher.forward(request, response);
			return;
			}
		}


//ヒーローが攻撃したかどうか確かめる
		@SuppressWarnings("unused")
		//ヒーローがアッタクしたときはHeroAttackeMessageのメッセージをJspから持ってこれるのでそれで判断
		Boolean isHeroAttacked = HeroAttackeMessage != ""
				? HeroAttackeMessage != ""
				:false;

		//モンスターの配列の順番を取得
		int turn = Integer.parseInt(request.getParameter("i"));
		// 攻撃対象のモンスターのIdを持ってくる
		String monsterId = request.getParameter("monsterId");

		//もしヒーローがまだ攻撃できていないようなら攻撃させる
		if (!isHeroAttacked) {

			MonsterBeans monster= MonsterDao.getMonsterById(monsterId);

			int damageToMonster = Attack.HeroAttack(turn, monsterId, monsterList,hero);
			request.setAttribute("HeroAttackeMessage", hero.getName()+" が "+ monster.getName() +turn +" に "+ damageToMonster+"のダメージをあたえた！");

			if(monsterList.get(i).getHp()<=0) {

				request.setAttribute("HeroAttackeMessage",damageToMonster+"のダメージをあたえた！" +   hero.getName()+" が "+ monsterList.get(i).getName() +turn +"をやっつけた！");
				monsterList.remove(monsterList.get(turn));
				System.out.println("敵をやっつけた");
				System.out.println();

				}
		}



//							for(int i1 = 0; i1<StatusList.size(); i1++) {
//
//								if(hero.getGetexp( ) >= StatusList.get(i1).getNeedExp() && hero.getLv() == StatusList.get(i1).getLv()){
//
//									System.out.println("レベルアップおめでとう！");
//
//									int restExp =(int) (hero.getGetexp( ) - StatusList.get(i1).getNeedExp());
//
//									// ヒーローの総合計経験値をレベルアップしたら０に戻す
//									hero.setGetexp(restExp);
//									hero.setLv( StatusList.get(i1).getLv());
//
//								}
//							}








/*--------------------------------------------------------------------------------------------------------*/


