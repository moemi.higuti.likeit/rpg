package rpgGame;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.HeroBeans;
import beans.WeaponBeans;
import dao.WeaponDao;

/**
 * Servlet implementation class WeaponShop
 */
@WebServlet("/WeaponShop")
public class WeaponShop extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public WeaponShop() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");
		//htmlでつっくった画面を出す
		// フォワード　サーブレットからJSPに行く
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/weaponshop.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

		//オープニングで作ったヒーローインスタンスを取得
			HeroBeans hero =  (HeroBeans) session.getAttribute("hero");
			//もしヒーローインスタンスがとれなかったらオープニング画面に戻る
			if (hero == null) {
				response.sendRedirect("Opening");
				return;
			}

			//JSPかたメッセージを受け取る
			String ItemMessage =request.getParameter("ItemMessage");

	/*--------------------------------------------------------------------------------------------------------*/

			//武器屋やの持っている情報をセッションで取得
			ArrayList<WeaponBeans> WeaponShopList = (ArrayList<WeaponBeans>) session.getAttribute("WeaponShopList");

			//ヒーローが持っているアイテム(あらかじめデータベースにあった情報をもとに作る)の情報をセッションで取得
			ArrayList<WeaponBeans> HeroHoldWeaponList = (ArrayList<WeaponBeans>) session.getAttribute("HeroHoldWeaponList");

			if (HeroHoldWeaponList == null) {
				HeroHoldWeaponList = new ArrayList<WeaponBeans>();
				session.setAttribute("HeroHoldWeaponList", HeroHoldWeaponList);
			}

			//買う武器場のIDを取得する
			int WeaponId =Integer.parseInt(request.getParameter("WeaponId"));

	/*--------------------------------------------------------------------------------------------------------*/

			try {
				WeaponBeans heroitem = WeaponDao.getWeaponById(WeaponId);

				//ヒーローが持っている道具のリストを作るもしも抜けがあったらそれを上から
//					ArrayList<HeroHoldBeans> HeroitemListAtItemShop = new ArrayList<HeroHoldBeans>();
//					for(int i = 0; i<9 ; i++) {
	//
//						int num = 1+i;
//						HeroHoldBeans heroitem = HeroHoldDao.getItemById(num);
	//
//						if(heroitem.getName()!=null) {
//							HeroitemListAtItemShop.add(heroitem);
//						}
//					}

				//買えなかった時の処理
				if(heroitem.getCost() > hero.getGold()) {

					System.out.println("ーーーーゴールドが足りませんーーーー");
					System.out.println();
					request.setAttribute("ItemMessage", "ゴールドが足りません。。。");

					//htmlでつっくった画面を出す
					// フォワード　サーブレットからJSPに行く
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/weaponshop.jsp");
					dispatcher.forward(request, response);
					return;

				}else if(HeroHoldWeaponList.size() > 0){

					System.out.println("ーーーーこれ以上持てませんーーーー");
					System.out.println();
					request.setAttribute("ItemMessage", "これ以上持てません。。。");

					//htmlでつっくった画面を出す
					// フォワード　サーブレットからJSPに行く
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/weaponshop.jsp");
					dispatcher.forward(request, response);
					return;

				}else {

					    HeroHoldWeaponList.add(heroitem);

						System.out.println("ーーーーアイテム買えたーーーー");
						System.out.println();

						//ヒーローの金額もその分減らしておく
						hero.setGold(hero.getGold()-heroitem.getCost());

						request.setAttribute("ItemMessage", "ご購入ありがとうございます！！！！！");

						session.setAttribute("HeroHoldWeaponList", HeroHoldWeaponList);

						//htmlでつっくった画面を出す
						// フォワード　サーブレットからJSPに行く
						RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/weaponshop.jsp");
						dispatcher.forward(request, response);

						return;

				}


			} catch (SQLException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}


		}


}
