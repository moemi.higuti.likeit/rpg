package rpgGame;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.HeroBeans;
import beans.HeroHoldBeans;
import beans.WeaponBeans;
import dao.HeroDao;

/**
 * Servlet implementation class Save
 */
@WebServlet("/Save")
public class Save extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Save() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

		//htmlでつっくった画面を出す
		// フォワード　サーブレットからJSPに行く
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/save.jsp");
		dispatcher.forward(request, response);
		return;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

		/*--------------------------------------------------------------------------------------------------------*/

		//オープニングで作ったヒーローインスタンスを取得
		HeroBeans hero = (HeroBeans) session.getAttribute("hero");

		//復活の呪文を取得
		String savePassword = request.getParameter("savePassword");

		//ヒーローが持っているアイテム(あらかじめデータベースにあった情報をもとに作る)の情報をセッションで取得
		ArrayList<HeroHoldBeans> HeroHoldList = (ArrayList<HeroHoldBeans>) session.getAttribute("HeroHoldList");

		//ヒーローが持っているアイテム(あらかじめデータベースにあった情報をもとに作る)の情報をセッションで取得
		ArrayList<WeaponBeans> HeroHoldWeaponList = (ArrayList<WeaponBeans>) session.getAttribute("HeroHoldWeaponList");

		// 復活の呪文並びパスワードがきちんと入っているか調べる
		if (savePassword == null || savePassword.equals("")) {
			request.setAttribute("ErroeMessage", "復活の呪文を入れてください");

			//htmlでつっくった画面を出す
			// フォワード　サーブレットからJSPに行く
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/save.jsp");
			dispatcher.forward(request, response);
			return;
		}
		// パスワードの重複をチェック
		try {
			if (HeroDao.isOverlapPassword(savePassword)) {
				request.setAttribute("ErroeMessage", "ちがう復活の呪文を入れてください");

				//htmlでつっくった画面を出す
				// フォワード　サーブレットからJSPに行く
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/save.jsp");
				dispatcher.forward(request, response);

				return;
			} else {

				if (HeroHoldWeaponList.size() > 0) {
					HeroDao.insertHero2withWeapon(hero, savePassword, HeroHoldList, HeroHoldWeaponList);

				} else {
					HeroDao.insertHero2(hero, savePassword, HeroHoldList, HeroHoldWeaponList);
				}

				request.setAttribute("FatherMessage", "おお　神よ！　　　この者に　あなたさまのご加護の　あらんことを！");
				session.setAttribute("hero", hero);

				//htmlでつっくった画面を出す
				// フォワード　サーブレットからJSPに行く
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/opening.jsp");
				dispatcher.forward(request, response);

			}
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

	}

}
