package rpgGame;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.HeroBeans;
import beans.HeroHoldBeans;
import beans.MonsterBeans;
import beans.StatusBeans;
import beans.WeaponBeans;
import dao.MonsterDao;
import dao.StatusDao;

/**
 * Servlet implementation class SpecialAttack
 */
@WebServlet("/SpecialAttack")
public class SpecialAttack extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SpecialAttack() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

/*--------------------------------------------------------------------------------------------------------*/

		//オープニングで作ったヒーローインスタンスを取得
		HeroBeans hero =  (HeroBeans) session.getAttribute("hero");
		//もしヒーローインスタンスがとれなかったらオープニング画面に戻る
		if (hero == null) {
			response.sendRedirect("Opening");
			return;
		}


/*--------------------------------------------------------------------------------------------------------*/

		//出てくるモンスターの情報をを取得
		ArrayList<MonsterBeans> monsterList = (ArrayList<MonsterBeans>) session.getAttribute("monsterList");

		//ヒーローが持っているアイテム(あらかじめデータベースにあった情報をもとに作る)の情報をセッションで取得
		ArrayList<HeroHoldBeans> HeroHoldList = (ArrayList<HeroHoldBeans>) session.getAttribute("HeroHoldList");

		//ヒーローが持っているアイテム(あらかじめデータベースにあった情報をもとに作る)の情報をセッションで取得
		ArrayList<WeaponBeans> HeroHoldWeaponList = (ArrayList<WeaponBeans>) session.getAttribute("HeroHoldWeaponList");



/*--------------------------------------------------------------------------------------------------------*/

		//背景を持ってくる
		String background = request.getParameter("background");
		session.setAttribute("background", background);

		//背景を持ってくる
		int specialAttackId = Integer.parseInt(request.getParameter("specialAttackId"));

		//背景を持ってくる
		if(specialAttackId == 2) {
			hero.setHp(hero.getHp()-10*monsterList.size());
			request.setAttribute("SpecialAttackeMessage", hero.getName()+"は技の反動を受けた");
		}


		//背景を持ってくる
		String specialAttackName = request.getParameter("specialAttackName");
/*--------------------------------------------------------------------------------------------------------*/


		//倒したモンスターリスト情報をセッションで取得
		ArrayList<MonsterBeans> DefeatMonsterList = (ArrayList<MonsterBeans>) session.getAttribute("DefeatMonsterList");

		//セッションに上の倒したモンスターがない場合倒したモンスターリストを作成
		if (DefeatMonsterList == null) {

			DefeatMonsterList = new ArrayList<MonsterBeans>();
			session.setAttribute("DefeatMonsterList", DefeatMonsterList);

		}


/*--------------------------------------------------------------------------------------------------------*/


			//上で取得した武器情報を通常攻撃に加算していく
			@SuppressWarnings("unused")
			int AdditionalAttack=0;

			if(HeroHoldWeaponList != null) {

				for(int i =0; i<HeroHoldWeaponList.size(); i++) {

					//一応持っている武器のアディショナル攻撃を加算していく
					AdditionalAttack += HeroHoldWeaponList.get(i).getAdditional_attack();

				}

			}

			String HeroAttackeMessage = hero.getName()+"は"+specialAttackName+"をした！<br>";
			//ヒーローがモンスターを攻撃するメソッド
			for(int i = monsterList.size()-1; i>=0; i--) {

					//見つけたらそのモンスターへのダメージ量を計算
					//0.82~それ以上の出るランダム関数を作る
					double attackRan=  (Math.random()*1.2)+specialAttackId*0.32;

					//基本のダメージ　を計算
					int baseattack = hero.getAttack() - monsterList.get(i).getDefense()+AdditionalAttack;

					System.out.println("余分攻撃は"+AdditionalAttack);

					//基本ダメージと乱数をかける
					int damageToMonster = (int) (baseattack * attackRan);

					if(damageToMonster<=0) {
						damageToMonster = 1;
					}

					System.out.println();
					System.out.println("-----モンスターが食らったダメージ"+damageToMonster);

					//上記のダメージ文をそのモンスターのHpを減らす
					monsterList.get(i).setHp(monsterList.get(i).getHp()-damageToMonster);

					HeroAttackeMessage += hero.getName()+" が "+ monsterList.get(i).getName()+" に "+ damageToMonster +"のダメージをあたえた！<br>";

					System.out.println();
					System.out.println("モンスターの残りのHp"+monsterList.get(i).getHp());
					System.out.println();

						//もしモンスターのHPが0になったらの処理をする
						if(monsterList.get(i).getHp()<=0) {
							HeroAttackeMessage +=  monsterList.get(i).getName()+ "をやっつけた！<br>";

							//倒したモンスターを倒したリストに入れる
							DefeatMonsterList.add(monsterList.get(i));

							//倒したモンスターを生存モンスターリストから外す
							monsterList.remove(monsterList.get(i));
							System.out.println("敵をやっつけた");
							System.out.println();
							session.setAttribute("DefeatMonsterList", DefeatMonsterList);
							session.setAttribute("HeroHoldList", HeroHoldList);

							}

					request.setAttribute("HeroAttackeMessage",HeroAttackeMessage);
			}
/*------------------------------------------モンスターを全員倒した場合--------------------------------------------*/

					//モンスター全部を倒した場合
					if(monsterList.size() == 0) {

						request.setAttribute("AnnihilationMessage","全ての敵を倒した");

/*------------------------------------------倒したモンスターの情報をゲットしていく--------------------------------------------*/

						int totalGold = MonsterDao.getTotalMonsterGold(DefeatMonsterList);
						int totalExp = MonsterDao.getTotalMonsterExp(DefeatMonsterList);

						hero.setGold(hero.getGold()+totalGold);
						hero.setGetexp(hero.getGetexp()+totalExp);

						request.setAttribute("MonsterAttackeMessage3",hero.getName()+"は"+ totalGold +"G　を手に入れた！");

						request.setAttribute("MonsterAttackeMessage2",hero.getName()+"は"+ totalExp +"の経験値を手に入れた！");

						//倒したモンスターのリストのセッションを切る
						session.removeAttribute("DefeatMonsterList");


						//レベルUPの処理 ステータス情報を取得

						try {
							//ステータス情報を変数に入れる
							ArrayList<StatusBeans> StatusList = StatusDao.getStatusInfo();

							if(hero.getGetexp( ) >= StatusList.get(hero.getLv()-1).getNeedExp()){

								System.out.println("レベルアップおめでとう！");

								int restExp =(int) (hero.getGetexp( ) - StatusList.get(hero.getLv()-1).getNeedExp());

								// レベルアップしたのでステータスを更新
								hero.setGetexp(restExp);
								hero.setLv(hero.getLv()+1);
								hero.setAttack(StatusList.get(hero.getLv()-1).getAttack());
								hero.setMax_hp(StatusList.get(hero.getLv()-1).getMax_hp());
								hero.setDefense(StatusList.get(hero.getLv()-1).getDefense());

								session.setAttribute("hero", hero);
								session.setAttribute("background", background);
								session.setAttribute("HeroHoldList", HeroHoldList);
								//もしもデータベースの情報を変えたいのならここで変えられる
								request.setAttribute("MonsterAttackeMessage1",hero.getName()+"はレベルアップした！");

					}


						} catch (SQLException e) {
							// TODO 自動生成された catch ブロック
							e.printStackTrace();
						}


					}

					//htmlでつっくった画面を出す
					// フォワード　サーブレットからJSPに行く
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/battle.jsp");
					dispatcher.forward(request, response);

					return;

			}


}
