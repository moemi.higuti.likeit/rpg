package rpgGame;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.HeroBeans;
import beans.WeaponBeans;
import dao.HeroDao;

/**
 * Servlet implementation class Update
 */
@WebServlet("/Update")
public class Update extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Update() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

		/*--------------------------------------------------------------------------------------------------------*/

		//オープニングで作ったヒーローインスタンスを取得
		HeroBeans hero = (HeroBeans) session.getAttribute("hero");

		//復活の呪文を取得
		String savePassword = request.getParameter("savePassword");

		//ヒーローが持っているアイテム(あらかじめデータベースにあった情報をもとに作る)の情報をセッションで取得
		ArrayList<WeaponBeans> HeroHoldWeaponList = (ArrayList<WeaponBeans>) session.getAttribute("HeroHoldWeaponList");

		// 復活の呪文並びパスワードがきちんと入っているか調べる
		if (savePassword == null || savePassword.equals("")) {
			request.setAttribute("ErroeMessage", "復活の呪文を入れてください");

			//htmlでつっくった画面を出す
			// フォワード　サーブレットからJSPに行く
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/church02.jsp");
			dispatcher.forward(request, response);
			return;
		} else if (!savePassword.equals(hero.getPassword())) {

			request.setAttribute("ErroeMessage", "復活の呪文が違います");

			//htmlでつっくった画面を出す
			// フォワード　サーブレットからJSPに行く
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/church02.jsp");
			dispatcher.forward(request, response);
			return;
		} else {

			//ヒーローが武器を持っていた時の処理
			if (HeroHoldWeaponList.size() > 0) {
				
				HeroDao.UpdateHerowithWeapon(hero, savePassword, HeroHoldWeaponList);
				request.setAttribute("FatherMessage", "たしかに　記録しましたぞ。おお　神よ！  この者に　あなたさまのご加護の　あらんことを！");

				session.setAttribute("hero", hero);

				//htmlでつっくった画面を出す
				// フォワード　サーブレットからJSPに行く
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/church02.jsp");
				dispatcher.forward(request, response);
				return;

			} else {
				HeroDao.UpdateHero(hero, savePassword, HeroHoldWeaponList);

				request.setAttribute("FatherMessage", "たしかに　記録しましたぞ。おお　神よ！  この者に　あなたさまのご加護の　あらんことを！");

				session.setAttribute("hero", hero);

				//htmlでつっくった画面を出す
				// フォワード　サーブレットからJSPに行く
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/church02.jsp");
				dispatcher.forward(request, response);
				return;
			}

		}
	}

}
