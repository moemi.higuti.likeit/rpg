package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.WeaponBeans;

public class WeaponDao {

	public static ArrayList<WeaponBeans> getWeaponAll() throws SQLException {

		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			System.out.println("getConnection");

			st = con.prepareStatement("select * from weapon");

			ResultSet rs = st.executeQuery();

			ArrayList<WeaponBeans> WeaponShopList = new ArrayList<WeaponBeans>();

			while (rs.next()) {

				WeaponBeans weapon = new WeaponBeans();

				weapon.setId(rs.getInt("id"));
				weapon.setName(rs.getString("name"));
				weapon.setAdditional_attack(rs.getInt("additional_attack"));
				weapon.setCost(rs.getInt("cost"));
				weapon.setIcon(rs.getString("icon"));

				WeaponShopList.add(weapon);
			}
			System.out.println("getAll weapon completed");
			return WeaponShopList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}


	/*
	 * IDをもとに武器の情報を取得
	 *
	 * */

	public static WeaponBeans getWeaponById(int WeaponId) throws SQLException {

		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			System.out.println("getConnection");

			st = con.prepareStatement("select * from weapon where id =?");
			st.setInt(1, WeaponId);
			System.out.println(st);

			ResultSet rs = st.executeQuery();

			WeaponBeans weapon = new WeaponBeans();
			if (rs.next()) {

				weapon.setId(rs.getInt("id"));
				weapon.setName(rs.getString("name"));
				weapon.setAdditional_attack(rs.getInt("additional_attack"));
				weapon.setCost(rs.getInt("cost"));
				weapon.setIcon(rs.getString("icon"));

			}
			System.out.println("get　Weapon you want completed");
			return weapon;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}











}
