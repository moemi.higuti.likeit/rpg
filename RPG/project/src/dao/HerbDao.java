package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.HerbBeans;

public class HerbDao {

	public static ArrayList<HerbBeans> getItemAll() throws SQLException {

		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			System.out.println("getConnection");

			st = con.prepareStatement("select * from herb");

			ResultSet rs = st.executeQuery();

			ArrayList<HerbBeans> itemShopList = new ArrayList<HerbBeans>();

			while (rs.next()) {

				HerbBeans item = new HerbBeans();

				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setRecovery(rs.getInt("recovery"));
				item.setCost(rs.getInt("cost"));
				item.setIcon(rs.getString("icon"));

				itemShopList.add(item);
			}
			System.out.println("getAllCommand completed");
			return itemShopList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static HerbBeans getItemById(int buyItemId) throws SQLException {

		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			System.out.println("getConnection");

			st = con.prepareStatement("select * from herb where id = ?");
			st.setInt(1, buyItemId);
			System.out.println(st);

			ResultSet rs = st.executeQuery();

			HerbBeans BuyItemInfo = new HerbBeans();

			if (rs.next()) {

				BuyItemInfo.setId(rs.getInt("id"));
				BuyItemInfo.setName(rs.getString("name"));
				BuyItemInfo.setRecovery(rs.getInt("recovery"));
				BuyItemInfo.setCost(rs.getInt("cost"));
				BuyItemInfo.setIcon(rs.getString("icon"));

			}
			System.out.println("getAllCommand completed");
			return BuyItemInfo;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

}
