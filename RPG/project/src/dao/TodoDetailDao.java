package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.TodoDetailBeans;

public class TodoDetailDao {

	/**
	 * 画面　街中で実行するコマンドを入手する
	 */

	public static ArrayList<TodoDetailBeans> getComandAtBattle(String todoId) throws SQLException {

		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			System.out.println("getConnection");

			st = con.prepareStatement("SELECT * FROM todo_detail  where todo_id = ? ORDER BY turn ASC");
			st.setString(1, todoId);

			ResultSet rs = st.executeQuery();

			ArrayList<TodoDetailBeans> todoDetailList = new ArrayList<TodoDetailBeans>();

			while (rs.next()) {

				TodoDetailBeans todo = new TodoDetailBeans();

				todo.setId(rs.getInt("id"));
				todo.setTodo_id(rs.getInt("todo_id"));
				todo.setContent(rs.getString("content"));
				todo.setTurn(rs.getInt("turn"));

				todoDetailList.add(todo);
			}
			System.out.println("getAllCommand completed");
			return todoDetailList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}


}
