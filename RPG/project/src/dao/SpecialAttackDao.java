package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.SpecialAttackBeans;

public class SpecialAttackDao {

	public static ArrayList<SpecialAttackBeans> getSpecialAttackList(int heroLv) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM special_attack where hero_lv <= ? ");
			st.setInt(1, heroLv);

			ResultSet rs = st.executeQuery();

			ArrayList<SpecialAttackBeans> SpecialAttackList = new ArrayList<SpecialAttackBeans>();

			while (rs.next()) {

				SpecialAttackBeans todo = new SpecialAttackBeans();

				todo.setId(rs.getInt("id"));
				todo.setAttack_name(rs.getString("attack_name"));
				todo.setHero_lv(rs.getInt("hero_lv"));

				SpecialAttackList.add(todo);
			}

			System.out.println("get a monster completed");

			return SpecialAttackList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

}
