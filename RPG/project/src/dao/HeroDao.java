package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.HeroBeans;
import beans.HeroHoldBeans;
import beans.WeaponBeans;

public class HeroDao {

	public static HeroBeans getHeroInfo() throws SQLException {

		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM hero where id = 1");

			ResultSet rs = st.executeQuery();

			HeroBeans hero = new HeroBeans();

			if (rs.next()) {

				hero.setId(rs.getInt("id"));
				hero.setName(rs.getString("name"));
				hero.setMax_hp(rs.getInt("max_hp"));
				hero.setHp(rs.getInt("hp"));
				hero.setAttack(rs.getInt("attack"));
				hero.setDefense(rs.getInt("defense"));
				hero.setLv(rs.getInt("lv"));
				hero.setGold(rs.getInt("gold"));

				hero.setEquip_weapon_id(rs.getInt("equip_weapon_id"));
				hero.setGetexp(rs.getInt("getexp"));

			}

			System.out.println("getHeroInfo completed");
			return hero;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}



	public static HeroBeans getHeroInfoById(int HeroId) throws SQLException {

		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM hero where id = ?");
			st.setInt(1, HeroId);


			ResultSet rs = st.executeQuery();

			HeroBeans hero = new HeroBeans();

			if (rs.next()) {

				hero.setId(rs.getInt("id"));
				hero.setName(rs.getString("name"));
				hero.setMax_hp(rs.getInt("max_hp"));
				hero.setHp(rs.getInt("hp"));
				hero.setAttack(rs.getInt("attack"));
				hero.setDefense(rs.getInt("defense"));
				hero.setLv(rs.getInt("lv"));
				hero.setGold(rs.getInt("gold"));

				hero.setEquip_weapon_id(rs.getInt("equip_weapon_id"));

				hero.setGetexp(rs.getInt("getexp"));

			}

			System.out.println("getHeroInfo completed");
			return hero;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}



	/**
	 * passwwordの重複チェック
	 *
	 * @param loginId
	 *            check対象のログインID
	 * @param userId
	 *            check対象から除外するuserID
	 * @return bool 重複している
	 * @throws SQLException
	 */
	public static boolean isOverlapPassword(String savePassword) throws SQLException {
		// 重複しているかどうか表す変数
		boolean isOverlap = false;
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			// 入力されたlogin_idが存在するか調べる
			st = con.prepareStatement("SELECT password FROM hero WHERE password =?");
			st.setString(1, savePassword);
			ResultSet rs = st.executeQuery();

			System.out.println("searching password has been completed");

			if (rs.next()) {
				isOverlap = true;
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

		System.out.println("overlap check has been completed");
		return isOverlap;
	}



	@SuppressWarnings("null")
	public static void insertHero(HeroBeans hero, String savePassword, ArrayList<HeroHoldBeans> heroHoldList,
			ArrayList<WeaponBeans> heroHoldWeaponList) throws SQLException {

		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			for(int i = 0; i<10; i++) {

				if(  heroHoldList.get(i).getName() == "" || heroHoldList.get(i).getName() == null) {
					int num = heroHoldList.get(i).getHerb_id();
					num = (Integer) null;

				}
			}



			st = con.prepareStatement("INSERT INTO hero ( `name`, `max_hp`, `hp`, `attack`, `defense`, `lv`, `gold`, `equip_weapon_id`,`hold_item_id1`,"
					+ " `hold_item_id2`, `hold_item_id3`, `hold_item_id4`, `hold_item_id5`,"
					+ " `hold_item_id6`, `hold_item_id7`, `hold_item_id8`, `hold_item_id9`, `hold_item_id10`, `getexp`, `password` ) "
					+ "VALUES(?,?, ?,?, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

			st.setString(1, hero.getName());
			st.setInt(2, hero.getMax_hp());
			st.setInt(3, hero.getHp());
			st.setInt(4, hero.getAttack());
			st.setInt(5, hero.getDefense());
			st.setInt(6, hero.getLv());
			st.setInt(7, hero.getGold());

			st.setInt(8, heroHoldWeaponList.get(1).getId());


			st.setInt(19, hero.getGetexp());
			st.setString(20,savePassword);

			st.executeUpdate();
			System.out.println("inserting user has been completed");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}




	}




	public static void insertHero2withWeapon(HeroBeans hero, String savePassword, ArrayList<HeroHoldBeans> heroHoldList,
			ArrayList<WeaponBeans> heroHoldWeaponList) throws SQLException {

		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("INSERT INTO hero ( `name`, `max_hp`, `hp`, `attack`, `defense`, `lv`, `gold`, `equip_weapon_id`,"
					+ " `getexp`, `password` ) "
					+ "VALUES(?,?, ?,?, ?,?,?,?,?,?)");

			st.setString(1, hero.getName());
			st.setInt(2, hero.getMax_hp());
			st.setInt(3, hero.getHp());
			st.setInt(4, hero.getAttack());
			st.setInt(5, hero.getDefense());
			st.setInt(6, hero.getLv());
			st.setInt(7, hero.getGold());

			st.setInt(8, heroHoldWeaponList.get(0).getId());

			st.setInt(9, hero.getGetexp());
			st.setString(10,savePassword);

			st.executeUpdate();
			System.out.println("inserting user has been completed");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}


	public static void insertHero2(HeroBeans hero, String savePassword, ArrayList<HeroHoldBeans> heroHoldList,
			ArrayList<WeaponBeans> heroHoldWeaponList) throws SQLException {

		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("INSERT INTO hero ( `name`, `max_hp`, `hp`, `attack`, `defense`, `lv`, `gold`,"
					+ " `getexp`, `password` ) "
					+ "VALUES(?,?, ?,?, ?,?,?,?,?)");

			st.setString(1, hero.getName());
			st.setInt(2, hero.getMax_hp());
			st.setInt(3, hero.getHp());
			st.setInt(4, hero.getAttack());
			st.setInt(5, hero.getDefense());
			st.setInt(6, hero.getLv());
			st.setInt(7, hero.getGold());

			st.setInt(8, hero.getGetexp());
			st.setString(9,savePassword);

			st.executeUpdate();
			System.out.println("inserting user has been completed");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}



	/**
	 * ログイン時の情報を取得する
	 *
	 * @param useId
	 *            ユーザーID
	 * @return udbList 引数から受け取った値に対応するデータを格納する
	 * @throws SQLException
	 *             呼び出し元にcatchさせるためスロー
	 */

	public static HeroBeans getHeroInfoByPassword(String password) throws SQLException {

		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM hero where password = ?");
			st.setString(1, password);

			ResultSet rs = st.executeQuery();


			HeroBeans hero = new HeroBeans();

			if (rs.next()) {

				hero.setId(rs.getInt("id"));
				hero.setName(rs.getString("name"));
				hero.setMax_hp(rs.getInt("max_hp"));
				hero.setHp(rs.getInt("hp"));
				hero.setAttack(rs.getInt("attack"));
				hero.setDefense(rs.getInt("defense"));
				hero.setLv(rs.getInt("lv"));
				hero.setGold(rs.getInt("gold"));

				hero.setEquip_weapon_id(rs.getInt("equip_weapon_id"));
				hero.setGetexp(rs.getInt("getexp"));
				hero.setPassword(rs.getString("password"));

			}

			System.out.println("getHeroInfo completed");
			return hero;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static void UpdateHerowithWeapon(HeroBeans hero, String savePassword,
			ArrayList<WeaponBeans> heroHoldWeaponList ) {

		Connection con = null;
		PreparedStatement st = null;

		try {
			// データベース接続
			con = DBManager.getConnection();

			// 実行SQL文字列定義
			st = con.prepareStatement("UPDATE hero SET  name=?, max_hp =?,hp=?, attack= ?,defense=?, lv =?, gold=?, equip_weapon_id =? , getexp=? WHERE password = ?");


			st.setString(1, hero.getName());
			st.setInt(2, hero.getMax_hp());
			st.setInt(3, hero.getHp());
			st.setInt(4, hero.getAttack());
			st.setInt(5, hero.getDefense());
			st.setInt(6, hero.getLv());
			st.setInt(7, hero.getGold());
			st.setInt(8, heroHoldWeaponList.get(0).getId());
			st.setInt(9, hero.getGetexp());
			st.setString(10,savePassword);

			st.executeUpdate();

			System.out.println("updating has been completed");

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {

				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public static void UpdateHero(HeroBeans hero, String savePassword,
			ArrayList<WeaponBeans> heroHoldWeaponList ) {

		Connection con = null;
		PreparedStatement st = null;

		try {
			// データベース接続
			con = DBManager.getConnection();

			// 実行SQL文字列定義
			st = con.prepareStatement("UPDATE hero SET  name=?, max_hp =?,hp=?, attack= ?,defense=?, lv =?, gold=?, equip_weapon_id =? , getexp=? WHERE password = ?");


			st.setString(1, hero.getName());
			st.setInt(2, hero.getMax_hp());
			st.setInt(3, hero.getHp());
			st.setInt(4, hero.getAttack());
			st.setInt(5, hero.getDefense());
			st.setInt(6, hero.getLv());
			st.setInt(7, hero.getGold());
			st.setInt(8, 0);
			st.setInt(9, hero.getGetexp());
			st.setString(10,savePassword);

			st.executeUpdate();

			System.out.println("updating has been completed");

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {

				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}


}
