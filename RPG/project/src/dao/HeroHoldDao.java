package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import base.DBManager;
import beans.HerbBeans;
import beans.HeroHoldBeans;

public class HeroHoldDao {

	public static HeroHoldBeans getItemById(int num) throws SQLException {

		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			System.out.println("getConnection");

			st = con.prepareStatement("select hero.id, herb.id, herb.name, herb.recovery,herb.cost,herb.icon from hero JOIN herb ON hero.hold_item_id"+num+" = herb.id");
			System.out.println(st);

			ResultSet rs = st.executeQuery();

			HeroHoldBeans heroHold = new HeroHoldBeans();

			if (rs.next()) {

				heroHold.setHero_id(Integer.parseInt(rs.getString("hero.id")));
				heroHold.setName(rs.getString("name"));
				heroHold.setRecovery(rs.getInt("recovery"));
				heroHold.setCost(rs.getInt("cost"));
				heroHold.setIcon(rs.getString("icon"));
				heroHold.setHerb_id(rs.getInt("herb_id"));

			}
			System.out.println("getAllCommand completed");
			return heroHold;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}


	public static HeroHoldBeans getItemByItemId(int buyItemId) throws SQLException {

		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			System.out.println("getConnection");

			st = con.prepareStatement("select hero.id, herb.id, herb.name, herb.recovery,herb.cost,herb.icon from hero JOIN herb ON hero.hold_item_id1 = herb.id where herb.id = ?");
			System.out.println(st);
			st.setInt(1, buyItemId);

			ResultSet rs = st.executeQuery();

			HeroHoldBeans heroHold = new HeroHoldBeans();

			if (rs.next()) {

				heroHold.setId(rs.getInt("herb.id"));

				heroHold.setHero_id(Integer.parseInt(rs.getString("hero.id")));
				heroHold.setName(rs.getString("name"));
				heroHold.setRecovery(rs.getInt("recovery"));
				heroHold.setCost(rs.getInt("cost"));
				heroHold.setIcon(rs.getString("icon"));
				heroHold.setHerb_id(rs.getInt("herb_id"));

			}
			System.out.println("getAllCommand completed");
			return heroHold;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}



	public static void ImsertItem(HeroHoldBeans heroitem, int heroId) throws SQLException {

		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("INSERT INTO hero_hold (`hero_id`  ,`name`,`recovery`, `cost`,`icon`,`herb_id`) VALUES (? , ?, ?,  ?, ?, ?)");

			st.setInt(1,heroId);
			st.setString(2, heroitem.getName());
			st.setInt(3, heroitem.getRecovery());
			st.setInt(4, heroitem.getCost());
			st.setString(5, heroitem.getIcon());
			st.setInt(6, heroitem.getId());

			st.executeUpdate();

			System.out.println("Item bought");


		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}

	public static void buyItem(HerbBeans heroitem, int heroId) throws SQLException {

		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("INSERT INTO hero_hold (`hero_id`  ,`name`,`recovery`, `cost`,`icon`,`herb_id`) VALUES (? , ?, ?,  ?, ?, ?)");

			System.out.println("getAllCommand completed");
			st.setInt(1,heroId);
			st.setString(2, heroitem.getName());
			st.setInt(3, heroitem.getRecovery());
			st.setInt(4, heroitem.getCost());
			st.setString(5, heroitem.getIcon());
			st.setInt(6, heroitem.getId());

			st.executeUpdate();
			System.out.println("Item bought");



		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}

	public static int CountLastHeroItem() throws SQLException {

		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT COUNT( * ) as cnt FROM hero_hold");

			System.out.println("getAllCommand completed");

			ResultSet rs = st.executeQuery();

				int lastNum = 0;
				while (rs.next()) {
					lastNum =  (int) Double.parseDouble(rs.getString("cnt"));
				}
				System.out.println("Get last HeroItemNum");

			return lastNum;




		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}


	}




	public static HeroHoldBeans getLastHeroHoldById(int lastNum) throws SQLException {

		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			System.out.println("getConnection");

			st = con.prepareStatement("select * from hero_hold where id = ?");
			System.out.println(st);
			st.setInt(1, lastNum);

			ResultSet rs = st.executeQuery();

			HeroHoldBeans heroHold = new HeroHoldBeans();

			if (rs.next()) {

				heroHold.setId(rs.getInt("id"));

				heroHold.setHero_id(Integer.parseInt(rs.getString("hero_id")));
				heroHold.setName(rs.getString("name"));
				heroHold.setRecovery(rs.getInt("recovery"));

				heroHold.setCost((int) (rs.getInt("cost") *3/4));
				heroHold.setIcon(rs.getString("icon"));
				heroHold.setHerb_id(rs.getInt("herb_id"));

			}
			System.out.println("getAllCommand completed");
			return heroHold;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}




}
