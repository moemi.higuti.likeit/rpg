package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.MonsterBeans;

public class MonsterDao {

	public static double getMonsterCount() throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT count(*) as cnt FROM monster");
			ResultSet rs = st.executeQuery();
			double coung = 0.0;

			while (rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}

			return coung;

		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static MonsterBeans getMonsterSingle(String todoId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM monster where place=? ORDER BY RAND() limit 1");
			st.setString(1, todoId);

			ResultSet rs = st.executeQuery();

			MonsterBeans monster = new MonsterBeans();

			if (rs.next()) {

				monster.setId(rs.getInt("id"));
				monster.setName(rs.getString("name"));
				monster.setHp(rs.getInt("hp"));
				monster.setAttack(rs.getInt("attack"));
				monster.setDefense(rs.getInt("defense"));
				monster.setReward(rs.getInt("reward"));
				monster.setIcon(rs.getString("icon"));
				monster.setExp(rs.getInt("exp"));

			}

			System.out.println("get a monster completed");

			return monster;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}


	/*
	 * idをもとにモンスターの情報を取得
	 * */
	public static MonsterBeans getMonsterById(String monsterId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM monster where id = ?");
			st.setString(1, monsterId);

			ResultSet rs = st.executeQuery();

			MonsterBeans monster = new MonsterBeans();

			if (rs.next()) {

				monster.setId(rs.getInt("id"));
				monster.setName(rs.getString("name"));
				monster.setHp(rs.getInt("hp"));
				monster.setAttack(rs.getInt("attack"));
				monster.setDefense(rs.getInt("defense"));
				monster.setReward(rs.getInt("reward"));
				monster.setIcon(rs.getString("icon"));

			}

			System.out.println("get a attacked monster completed");

			return monster;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 倒したモンスターの獲得ゴールドを算出する
	 *
	 */
	public static int getTotalMonsterGold(ArrayList<MonsterBeans> DefeatMonsterList) {
		int total = 0;
		for (MonsterBeans DefeatMonsterList1: DefeatMonsterList) {
			total += DefeatMonsterList1.getReward();
		}
		return total;
	}

	/**
	 * 倒したモンスターの獲得経験値を算出する
	 *
	 */

	public static int getTotalMonsterExp(ArrayList<MonsterBeans> DefeatMonsterList) {
		int total = 0;
		for (MonsterBeans DefeatMonsterList1: DefeatMonsterList) {
			total += DefeatMonsterList1.getExp();
		}
		return total;
	}




}
