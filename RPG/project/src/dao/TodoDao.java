package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.TodoBeans;

public class TodoDao {


	/**
	 * 画面　街中で実行するコマンドを入手する
	 * (外に出るときのコマンド)
	 */

	public static ArrayList<TodoBeans> ToDoListOut() throws SQLException {

		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			System.out.println("getConnection");

			st = con.prepareStatement("SELECT * FROM todo where place_id = 1  ORDER BY turn ASC");

			ResultSet rs = st.executeQuery();

			ArrayList<TodoBeans> todoList = new ArrayList<TodoBeans>();

			while (rs.next()) {

				TodoBeans todo = new TodoBeans();

				todo.setId(rs.getInt("id"));
				todo.setContent(rs.getString("content"));
				todo.setTurn(rs.getInt("turn"));
				todo.setPlace_id(rs.getInt("place_id"));
				todo.setPlace_icon(rs.getString("place_icon"));

				todoList.add(todo);
			}
			System.out.println("getAllCommand completed");
			return todoList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 画面　街中で実行するコマンドを入手する
	 * (街中散策に出るときのコマンド)
	 */


	public static ArrayList<TodoBeans> ToDoListInTown() throws SQLException {

		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			System.out.println("getConnection");

			st = con.prepareStatement("SELECT * FROM todo where place_id = 2  ORDER BY turn ASC");

			ResultSet rs = st.executeQuery();

			ArrayList<TodoBeans> todoList = new ArrayList<TodoBeans>();

			while (rs.next()) {

				TodoBeans todo = new TodoBeans();

				todo.setId(rs.getInt("id"));
				todo.setContent(rs.getString("content"));
				todo.setTurn(rs.getInt("turn"));
				todo.setPlace_id(rs.getInt("place_id"));
				todo.setPlace_icon(rs.getString("place_icon"));

				todoList.add(todo);
			}
			System.out.println("getAllCommand completed");
			return todoList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}


}
