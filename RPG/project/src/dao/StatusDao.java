package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.StatusBeans;

public class StatusDao {

	public static  ArrayList<StatusBeans> getStatusInfo() throws SQLException {

		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("select * from status");

			ResultSet rs = st.executeQuery();

			ArrayList<StatusBeans> statusList = new ArrayList<StatusBeans>();

			while (rs.next()) {

				StatusBeans status = new StatusBeans();

				status.setId(rs.getInt("id"));
				status.setLv(rs.getInt("lv"));
				status.setNeedExp(rs.getInt("needExp"));
				status.setMax_hp(rs.getInt("max_hp"));
				status.setAttack(rs.getInt("attack"));
				status.setDefense(rs.getInt("defense"));

				statusList.add(status);

			}

			System.out.println("getHeroInfo completed");
			return statusList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

}
