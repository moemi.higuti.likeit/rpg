<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" >
    <title>武器屋</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/monster.css">
    <link rel="stylesheet" href="css/item.css">
    <link rel="stylesheet" href="css/gotoshop.css">
    <script src="jquery.min.js"></script>
    <Style>
        body{
            background:url(img/weaponshop.jpg);
            background-repeat: no-repeat;
            background-size: cover;
        }
    </Style>
</head>
<body>
    <main>
        <section>

            <div class="paramater">
                <ul>
                    <li>名前　　　：　${hero.name}</li>
                    <li>HP　　　 ：　${hero.hp}　</li>
                    <li>レベル　　：　${hero.lv}</li>
                    <li>ゴールド　：　${hero.gold}　G</li>
                </ul>
            </div>

            <div class="control col-md-10 row">
                <div class="col-md-3 comand">
                    <ul>
	                    <c:if test="${ItemMessage == null}">
	                        <li id="showbuy" >買い物をする</li>
	                        <li id="showsell">道具を売る</li>
	                        <a href="Town"><li>街中に戻る</li></a>
	                        <li id="nomeaning">店主と話す</li>
	                    </c:if>
                    </ul>
                </div>
                <div class="col-md-9 texts">
                    <p class="talker">武器屋の店主 : </p>
                     <c:if test="${ItemMessage == null}">
                   		<p id="a">「　いらっしゃいませ！ ご用件は何でしょうか？　」</p>
                   		<br>
					</c:if>


	                    <c:if test="${ItemMessage != null}">
	                   		${ItemMessage}<br><br><br>
	                    <button>
	                        <a href = "WeaponShop"><img src="img/arrow.jpg" alt=""></a>
	                    </button>
	                    </c:if>
                </div>
            </div>
        </section>
<!--  -----------------アイテムを買うセクション                                       ---------- -->

        <section>
            <div class="itembuy itemcollections">
                <div class="itemselect">
                    <ul>

                        <c:forEach var="weapon" items="${WeaponShopList}">
		                    <form action="WeaponShop" method="POST">
		                        <button>

			                            <li class="row">
			                                <img class="mr-3" src="img/${weapon.icon}" alt="">
			                                <div class="text mx-2 row">
			                                <input type="hidden" name="WeaponId" value="${weapon.id}">
			                                    <p class="mx-2" >${weapon.name}</p>
			                                    <span class="mx-3" >${weapon.cost}G</span>
			                                </div>
			                            </li>

		                        </button>
		                     </form>
	                       </c:forEach>


                    </ul>

                    <div class="back">
                        <p class="showdown">買い物をやめる</p>
                    </div>
                </div>
            </div>
        </section>

<!------------------------------------ 道具を売るリスト----------------------------------------------------->
        <section>
            <div class="itemsell itemcollections">
                <div class="itemselect">
                    <ul>

                        <c:if test="${HeroHoldWeaponList != NULL && HeroHoldWeaponList.size() != 0}">
						<c:forEach begin="0" var="i" end="${HeroHoldWeaponList.size()-1}" step="1">
							<form action="WeaponShopSell" method="POST">

							<input type="hidden" name="holdId" value="${HeroHoldWeaponList.get(i).id}">
							<input type="hidden" name="i" value="${i}">
							<input type="hidden" name="Cost" value="${HeroHoldWeaponList.get(i).cost}">

	                        <button>

	                            <li class="row">
	                                <img class="mr-3" src="img/${HeroHoldWeaponList.get(i).icon}" alt="">
	                                <div class="text mx-2 row">

	                                    <p class="mx-2" >${HeroHoldWeaponList.get(i).name}</p>
	                                    <span class="mx-3" >${HeroHoldWeaponList.get(i).cost} G</span>
	                                </div>
	                            </li>

	                        </button>
	                         </form>
						</c:forEach>
						</c:if>
						<c:if test="${HeroHoldWeaponList == NULL}">
							<div>
		                        <p>武器がありません</p>
		                    </div>
						</c:if>

                    </ul>

                    <div class="back">
                        <p class="showdown">やめる</p>
                    </div>
                </div>
            </div>
        </section>

    </main>
    <script src="common.js"></script>
    <script>
        //アイテムを買うときの画面の処理
        $(function(){
            $('#showbuy').click(function(){
            $('.itembuy').fadeIn();
            });
            $('.showdown').click(function(){
            $('.itembuy').fadeOut();
            });
        })

         //アイテムを売るときの画面の処理
        $(function(){
            $('#showsell').click(function(){
            $('.itemsell').fadeIn();
            });
            $('.showdown').click(function(){
            $('.itemsell').fadeOut();
            });
        })
    </script>
</body>
</html>