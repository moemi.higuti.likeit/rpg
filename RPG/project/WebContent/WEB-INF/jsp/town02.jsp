<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" >
    <title>街中</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/monster.css">
    <link rel="stylesheet" href="css/item.css">
    <link rel="stylesheet" href="css/gotoshop.css">
    <script src="jquery.min.js"></script>
    <style>
        body{
            background:url(img/town.jpg);
            background-repeat: no-repeat;
            background-size: cover;
        }
    </style>
</head>
<body>
    <main>
        <section>
            <div class="paramater">
                <ul>
					<li>名前　　　：${hero.name}</li>
                    <li>HP　　　 ：　${hero.hp}　</li>
                    <li>レベル　　：　${hero.lv}</li>
                    <li>ゴールド　：　${hero.gold}　G</li>
                    <li>経験値　：　${hero.getexp}　</li>
                </ul>
            </div>
            <div class="control col-md-10 row">
                <div class="col-md-3 comand">
                    <ul>
                        <li id="goout">戦いに行く</li>
                        <li id="intown">町を散策する</li>
                        <li id="status">ステータスを見る</li>
                        <li>自問自答する</li>
                    </ul>
                </div>
                <div class="col-md-9 texts">
                    <p id="a">
	                    <c:if test="${HeroAttackeMessage != null}">
		                     		${HeroAttackeMessage}
		                     		<br>
		                     		<br>
						</c:if>
						<c:if test="${HeroAttackeMessage == null}">
		                     		さてどうしようかな？
		                     		<br>
		                     		<br>
						</c:if>
					</p>
                    <button>
                        <img src="img/arrow.jpg" alt="">
                    </button>
                </div>
            </div>
        </section>

<!--ーーーーーーーーーーー外に出た時のモデルーーー  -->
<section>
    <div class="goout itemcollections">
        <div class="itemselect">
            <ul>

                     	<c:forEach var="todo" items="${ToDoListOut}">
		           		  <a href="Transition?todoId=${todo.id}">
		           		  <li class="row">
		                        <img class="mr-3" src="img/${todo.place_icon}" alt="">
		                        <div class="text mx-2 row">
		                            <p class="mx-2" >${todo.content}</p>
		                        </div>
                         </li>
                          </a>
	                 	</c:forEach>

            </ul>

            <div class="back">
                <p class="showdown">やめる</p>
            </div>
        </div>
    </div>
</section>



    <!--ーーーーーーーーーーーーーー  街中散策の場所を選ぶセクション　ーーーーーーーーー-->
    <section>
        <div class="intown itemcollections">
            <div class="itemselect">
                <ul>

                     	<c:forEach var="todo" items="${ToDoListInTown}">

		           		  <a href="Transition?todoId=${todo.id}">
		           		  	<li class="row">
		                        <img class="mr-3" src="img/${todo.place_icon}" alt="">
		                        <div class="text mx-2 row">
		                            <p class="mx-2" >${todo.content}</p>
		                        </div>
	                        </li>
                          </a>

	                 	</c:forEach>


                </ul>

                <div class="back">
                    <p class="showdown">やめる</p>
                </div>
            </div>
        </div>
    </section>

<!--ーーーーーーーーーーーステータス画面表示ーーーーーーーーーーーー-->
    <section class="statusList itemcollections">

        <div>
            <div class="itemselect row col-md-8">
                <div class="status col-md-5">
                    <ul>
                        <li>名前　　　：${hero.name}</li>
	                    <li>HP　　　 ：　${hero.hp}　</li>
	                    <li>レベル　　：　${hero.lv}</li>
	                    <li>ゴールド　：　${hero.gold}　G</li>
                        <li>攻撃力　　　：${hero.attack}</li>
                        <li>防御力　　　：　${hero.defense}　</li>
                        <li>最大HP　　：　${hero.max_hp}</li>

<!-- 　　　　　　　　　　　　　　　　ステータス武器　　　　　　　　　-->

						<c:if test="${HeroHoldWeaponList != NULL && HeroHoldWeaponList.size() != 0}">

	                        <c:forEach begin="0" var="i" end="${HeroHoldWeaponList.size()-1}" step="1">

	                            <li class="row">
	                                <img class="mr-3" src="img/${HeroHoldWeaponList.get(i).icon}" alt="">
	                                <div class="text mx-2 row">

	                                    <p class="mx-2" >${HeroHoldWeaponList.get(i).name}</p>
	                                </div>
	                            </li>
	                        </c:forEach>
						</c:if>
						<c:if test="${HeroHoldWeaponList.size() == 0}">
							<div>
		                        <p>武器がありません</p>
		                    </div>
						</c:if>
<!-- 　　　　　　　　　　　　　　　　ステータス　戻る　　　　　　　　　-->
                        <div class="back">
                            <p class="showdown">もどる</p>
                        </div>
                    </ul>
                </div>


<!-- 　　　　　　　　　　　　　　　　ステータス　道具一覧 　　　　　　　　　　　　　-->


				<ul class="row">
                        <div class="mr-3 itemselect-column">

                            <c:if test="${HeroHoldList != NULL  && HeroHoldList.size() != 0}">
							<c:forEach begin="0" var="i" end="${HeroHoldList.size()-1}" varStatus="status" step="1">
							<form action="UseItem" method="POST">

							<input type="hidden" name="holdId" value="${HeroHoldList.get(i).id}">
							<input type="hidden" name="i" value="${i}">
							<input type="hidden" name="Cost" value="${HeroHoldList.get(i).cost}">
							<input type="hidden" name="Recovery" value="${HeroHoldList.get(i).recovery}">
							<input type="hidden" name="jsp" value="town02">
	                        <button>

	                            <li class="row">
	                                <img class="mr-3" src="img/${HeroHoldList.get(i).icon}" alt="">
	                                <div class="text mx-2 row">

	                                    <p class="mx-2" >${HeroHoldList.get(i).name}</p>
	                                </div>
	                            </li>

	                        </button>

                        <!--  2nd row -->

                        <c:if test="${(status.index + 1) % 5 == 0}">

							</div>
	                        <div class="ml-3 itemselect-column">

						</c:if>
						 </form>
						</c:forEach>

						</div>
                    </ul>


                    </c:if>

						<c:if test="${HeroHoldList.size() == 0 || HeroHoldList == NULL}">
							<div>
		                        <p>アイテムがありません</p>
		                    </div>
						</c:if>

            </div>
        </div>
    </section>

</main>
    <script src="common.js"></script>
    <script>

        //ステータスを見るときの画面の処理
        $(function(){
            $('#status').click(function(){
            $('.statusList').fadeIn();
            });
            $('.showdown').click(function(){
            $('.statusList').fadeOut();
            });
        })

         //外に出る時のの画面の処理
         $(function(){
            $('#goout').click(function(){
            $('.goout').fadeIn();
            });
            $('.goout').click(function(){
            $('.goout').fadeOut();
            });
        })

        //町を散策する時の画面の処理
        $(function(){
            $('#intown').click(function(){
            $('.intown').fadeIn();
            });
            $('.intown').click(function(){
            $('.intown').fadeOut();
            });
        })




    </script>
</body>
</html>