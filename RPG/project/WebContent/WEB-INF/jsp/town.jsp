<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" >
    <title>街中</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/monster.css">
    <link rel="stylesheet" href="css/item.css">
    <link rel="stylesheet" href="css/gotoshop.css">
    <script src="jquery.min.js"></script>
    <style>
        body{
            background:url(img/town.jpg);
            background-repeat: no-repeat;
            background-size: cover;
        }
    </style>
</head>
<body>
    <main>
        <section>
            <div class="paramater">
                <ul>
					<li>名前　　　：${hero.name}</li>
                    <li>HP　　　 ：　${hero.hp}　</li>
                    <li>レベル　　：　${hero.lv}</li>
                    <li>ゴールド　：　${hero.gold}　G</li>
                    <li>経験値　：　${hero.getexp}　</li>

                </ul>
            </div>
            <div class="control col-md-10 row">
                <div class="col-md-3 comand">
                    <ul>
                    	<c:forEach var="todo" items="${ToDoList}">
	                        <a href="Transition?todoId=${todo.id}"><li>${todo.content}</li></a>
                        </c:forEach>
                    </ul>
                </div>
                <div class="col-md-9 texts">
                    <p id="a">今日はどうしようか？</p>
                    <button>
                        <img src="img/arrow.jpg" alt="">
                    </button>
                </div>
            </div>
        </section>
    </main>
    <script src="common.js"></script>
</body>
</html>