<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" >
    <title>戦闘</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/monster.css">
    <link rel="stylesheet" href="css/item.css">
    <script src="jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.2.6/gsap.min.js"></script>


	     <style>
        .bg {
            position: absolute;
            left: 0%;
            width: 0%;
            height: 100%;
            background:url(img/${background}.jpg);/* mountain.jpg , forest.jpg ,forest01.jpg */
            background-repeat: no-repeat;
            background-size: cover;
            z-index: -1;
        }

    </style>


</head>
<body>
    <main>
        <div class="bg"></div>

        	<section>
	            <div class="paramater">
	                <ul>
	                    <li>名前　　　：　${hero.name}</li>
	                    <li>HP　　　 ：　${hero.hp}　</li>
	                    <li>レベル　　：　${hero.lv}</li>
	                    <li>ゴールド　：　${hero.gold}　G</li>
	                </ul>
	            </div>

<!-- -　　　　　　　　　　　　　　-- モンスターを出現----　　　　　　　　　　- -->

	            <div class="col-md-12 monster">
	                <ul>
	                    <li>
	                    	<c:forEach var="monster" items="${monsterList}">
		                        <img  class="col-md-2" src="img/${monster.icon}" alt="">
	                        </c:forEach>
	                    </li>
	                </ul>
	            </div>


<!-- -　　　　　　　　　　　　　　--　コマンドの表示 ----　　　　　　　　　　- -->

	            <div class="control col-md-10 row">

	                <div class="col-md-3 comand">

		                    <ul>
		                    <c:if test="${HeroAttackeMessage == null}">

			                    	<c:forEach var="todo" items="${todoDetailList}">
				                        <button id="${todo.turn}"><li>${todo.content}</li></button>
									</c:forEach>
							</c:if>
		                    </ul>

	                </div>

<!-- -　　　　　　　　　　　　　　--　状態メッセージの表示 ----　　　　　　　　　　- -->

	                <div class="col-md-9 texts">
	                    <p id="a">

	                    	<c:if test="${AttackeMessage != null}">
	                     		${AttackeMessage}<br>
							</c:if>

							<c:if test="${SpecialAttackeMessage != null}">
	                     		${SpecialAttackeMessage}<br>
							</c:if>

	                    	<c:if test="${HeroAttackeMessage != null}">
	                     		${HeroAttackeMessage}<br>
	                     		<br>
							</c:if>

							<c:if test="${GameOverMessage != null}">
	                     		${GameOverMessage}<br>
							</c:if>

							<c:if test="${MonsterAttackeMessage0 != null}">
	                     		${MonsterAttackeMessage0}<br>
							</c:if>

							<c:if test="${MonsterAttackeMessage1 != null}">
	                     		${MonsterAttackeMessage1}<br>
							</c:if>

							<c:if test="${MonsterAttackeMessage2 != null}">
	                     		${MonsterAttackeMessage2}<br>
							</c:if>

							<c:if test="${MonsterAttackeMessage3 != null}">
	                     		${MonsterAttackeMessage3}<br>
							</c:if>

							<c:if test="${MonsterAttackeMessage4 != null}">
	                     		${MonsterAttackeMessage4}<br>
							</c:if>

							<c:if test="${MonsterAttackeMessage4 != null}">
	                     		${MonsterAttackeMessage4}<br>
							</c:if>

							<c:if test="${AnnihilationMessage != null}">
	                     		${AnnihilationMessage}<br>
							</c:if>

						</p>

						<form action="Battle" method="POST">

							<input type="hidden" name="background" value="${background}">
							<input type="hidden" name="AnnihilationMessage" value="${AnnihilationMessage}">
		                    <input type="hidden" name="HeroAttackeMessage" value="${HeroAttackeMessage}">
							<input type="hidden" name="GameOverMessage" value="${GameOverMessage}">
		                    <input type="hidden" name="MonsterAttackeMessage" value="${MonsterAttackeMessage}">
							<c:if test="${HeroAttackeMessage != null ||AnnihilationMessage != null || GameOverMessage != null }">
		                    	<button type="submit" name="action">
			                        <img src="img/arrow.jpg" alt="">
			                    </button>
		                    </c:if>
	                    </form>

	                </div>
	            </div>
	        </section>


<!--ーーーーーーーーーーーーーー  攻撃対象のモンスターを選ぶセクション　ーーーーーーーーー-->

        <section>
            <div class="itemsell itemcollections">
                <div class="itemselect">
                    <ul>
					<c:if test="${AnnihilationMessage == NULL}">

						<c:forEach begin="0" var="i" end="${monsterList.size()-1}" step="1">
							<form action="Battle" method="POST">

							<input type="hidden" name="monsterId" value="${monsterList.get(i).id}">
							<input type="hidden" name="i" value="${i}">
							<input type="hidden" name="background" value="${background}">
							<input type="hidden" name="AnnihilationMessage" value="${AnnihilationMessage}">
		                    <input type="hidden" name="HeroAttackeMessage" value="${HeroAttackeMessage}">
		                    <input type="hidden" name="MonsterAttackeMessage" value="${MonsterAttackeMessage}">

	                        <button type="submit" name="action">
	                            <li class="row">

	                                <img class="mr-3" src="img/${monsterList.get(i).icon}" alt="">
	                                <div class="text mx-2 row">

	                                    <p class="mx-2" >${monsterList.get(i).name}　${i}</p>
	                                </div>
                             </form>
	                            </li>
							</button>
                           </c:forEach>
					</c:if>
                    </ul>

                    <div class="back">
                        <p class="showdown">やめる</p>
                    </div>
                </div>
            </div>
        </section>

<!--ーーーーーーーーーーーーーー使う道具を選ぶセクションーーーーーーーーー-->
        <section>
            <div class="itemlist itemcollections">
                <div class="itemselect">
                    <ul class="row">
                        <div class="mr-3 itemselect-column">

                            <c:if test="${HeroHoldList != NULL  && HeroHoldList.size() != 0}">
							<c:forEach begin="0" var="i" end="${HeroHoldList.size()-1}" varStatus="status" step="1">
							<form action="UseItem" method="POST">

							<input type="hidden" name="holdId" value="${HeroHoldList.get(i).id}">
							<input type="hidden" name="i" value="${i}">
							<input type="hidden" name="Cost" value="${HeroHoldList.get(i).cost}">
							<input type="hidden" name="Recovery" value="${HeroHoldList.get(i).recovery}">
							<input type="hidden" name="jsp" value="battle">
	                        <button>

	                            <li class="row">
	                                <img class="mr-3" src="img/${HeroHoldList.get(i).icon}" alt="">
	                                <div class="text mx-2 row">

	                                    <p class="mx-2" >${HeroHoldList.get(i).name}</p>
	                                </div>
	                            </li>

	                        </button>

                        <!--  2nd row -->

                        <c:if test="${(status.index + 1) % 5 == 0}">

							</div>
	                        <div class="ml-3 itemselect-column">

						</c:if>
						 </form>
						</c:forEach>

						</div>
                    </ul>
                    </c:if>

                        <c:if test="${HeroHoldList == NULL  || HeroHoldList.size() == 0}">
							<div>
		                        <p>アイテムがありません</p>
		                    </div>
						</c:if>
                    </ul>


                    <div class="back">
                        <p class="showdown">やめる</p>
                    </div>

                </div>
            </div>
        </section>

<!----------------------------特技を選ぶセクション-------------------------------------- -->

<section>
    <div class="specialattack itemcollections">
        <div class="itemselect">
            <ul>

					<c:forEach var="todo" items="${SpecialAttackList}">
					<form action="SpecialAttack" method="POST">
		                <button>
		                    <li class="row">
		                        <div class="text mx-2 row">
		                        <input type="hidden" name="background" value="${background}">
		                        	<input type="hidden" name="specialAttackId" value="${todo.id}">
		                            <input type="hidden" name="specialAttackName" value="${todo.attack_name}">
		                            <p class="mx-2" >${todo.attack_name}</p>
		                        </div>
		                    </li>
		                </button>
		                 </form>
	                </c:forEach>


                <c:if test="${SpecialAttackList == NULL  || SpecialAttackList.size() == 0}">
					<div>
                        <p>何も習得していません</p>
                    </div>
				</c:if>

            </ul>

            <div class="back">
                <p class="showdown">やめる</p>
            </div>
        </div>
    </div>
</section>


<!--ーーーーーーーーーーーーーー  逃げるセクション　ーーーーーーーーー-->

        <section>
            <div class="runAway itemcollections">
                <div class="itemselect">

					<c:if test="${AnnihilationMessage == NULL}">

							<form action="RunAway" method="POST">

								<input type="hidden" name="i" value="${i}">
		                        <button type="submit" name="action">

									<ul>
										<li> <p>本当に逃げてもよろしいですか？</p></li>
									</ul>

								</button>
							</form>
					</c:if>


                    <div class="back">
                        <p class="showdown">やめる</p>
                    </div>
                </div>
            </div>
        </section>

<!--ーーーーーーーーーーーーーーメイン終了ーーーーーーーーー-->




    </main>
    <c:if test="${AttackeMessage != null}">
	     <script src="common.js"></script>
	</c:if>



    <script>
        //使うアイテムを買うときの画面の処理
        $(function(){
            $('#3').click(function(){
            $('.itemlist').fadeIn();
            });
            $('.showdown').click(function(){
            $('.itemlist').fadeOut();
            });
        })

         //アイテムを売るときの画面の処理
         $(function(){
            $('#1').click(function(){
            $('.itemsell').fadeIn();
            });
            $('.showdown').click(function(){
            $('.itemsell').fadeOut();
            });
        })

      	//使うアイテムを買うときの画面の処理
        $(function(){
            $('#4').click(function(){
            $('.runAway').fadeIn();
            });
            $('.showdown').click(function(){
            $('.runAway').fadeOut();
            });
        })

        $(function(){
            $('#2').click(function(){
            $('.specialattack').fadeIn();
            });
            $('.showdown').click(function(){
            $('.specialattack').fadeOut();
            });
        })

        TweenMax.to(".bg", .3, {
        	  width: "100%",
        	  })





    </script>
</body>
</html>