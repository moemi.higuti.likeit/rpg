<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" >
    <title>教会</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/monster.css">
    <link rel="stylesheet" href="css/item.css">
    <script src="jquery.min.js"></script>
    <style>
        *{
            color:red;

        }
        .paramater,
        .comand,
        .texts{
            border: 3px solid red !important;
        }

        body{
            background:url(img/church.jpg);
            background-repeat: no-repeat;
            background-size: cover;
        }
    </style>
</head>
<body>
    <main>
        <section>

            <div class="paramater">
                <ul>
                    <li>名前　　　：${hero.name}</li>
                    <li>HP　　　 ：　${hero.hp}　</li>
                    <li>レベル　　：　${hero.lv}</li>
                    <li>ゴールド　：　${hero.gold}　G</li>
                </ul>
            </div>

            <div class="control col-md-10 row">
                <div class="col-md-3 comand">
                    <ul>
                        <li id="showbuy" >おいのりをする</li>
                        <li id="showsell">おつげをきく</li>
                        <a href="Town"><li>街中に戻る</li></a>
                        <li id="nomeaning">神父と話す</li>
                    </ul>
                </div>
                <div class="col-md-9 texts">
                    <p class="talker">神父 : </p>
                    <p id="a">「生きとし生けるものはみな神の子。わが教会にどんな　ご用かな？ 」</p>
                    <button>
                        <img src="img/arrow.jpg" alt="">
                    </button>
                </div>
            </div>
        </section>

        <section>
            <div class="itembuy itemcollections">
                <div class="itemselect">
                    <ul>

                        <button>
                            <li class="row">
                                <div class="text mx-2 row">
                                    <p class="mx-2" >ぼうけんの書</p>
                                </div>
                            </li>
                        </button>

                        <button>
                            <li class="row">
                                <div class="text mx-2 row">
                                    <p class="mx-2" >すごい薬草</p>
                                </div>
                            </li>
                        </button>



                    </ul>

                    <div class="back">
                        <p class="showdown">買い物をやめる</p>
                    </div>
                </div>
            </div>
        </section>

        <!-- 道具を売るリスト-->
        <section>
            <div class="itemsell itemcollections">
                <div class="itemselect">
                    <ul>

                        <button>
                            <li class="row">
                                <img class="mr-3" src="img/yakusou02.png" alt="">
                                <div class="text mx-2 row">
                                    <p class="mx-2" >すごい薬草</p>
                                    <span class="mx-3" >10G</span>
                                </div>
                            </li>
                        </button>

                        <button>
                            <li class="row">
                                <img class="mr-3" src="img/yakusou02.png" alt="">
                                <div class="text mx-2 row">
                                    <p class="mx-2" >すごい薬草</p>
                                    <span class="mx-3" >10G</span>
                                </div>
                            </li>
                        </button>

                    </ul>

                    <div class="back">
                        <p class="showdown">やめる</p>
                    </div>
                </div>
            </div>
        </section>

    </main>
    <script src="common.js"></script>
    <script>
        //アイテムを買うときの画面の処理
        $(function(){
            $('#showbuy').click(function(){
            $('.itembuy').fadeIn();
            });
            $('.showdown').click(function(){
            $('.itembuy').fadeOut();
            });
        })

         //アイテムを売るときの画面の処理
        $(function(){
            $('#showsell').click(function(){
            $('.itemsell').fadeIn();
            });
            $('.showdown').click(function(){
            $('.itemsell').fadeOut();
            });
        })
    </script>
</body>
</html>