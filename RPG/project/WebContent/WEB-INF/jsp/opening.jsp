<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" >
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.2.6/gsap.min.js"></script>
    <title>ゲームスタート</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/opening.css">
    <link rel="stylesheet" href="css/gotoshop.css">
</head>
<body>
    <main>
        <div id="left_bg">
            <div class="left_over"></div>
        </div>
        <div id="right_bg">
            <div class="left_over"></div>
        </div>
        <section>

           <div id="heading">
            <h1><span>R</span>
                <span>P</span>
                <span>G</span>
                <span></span>
                <span  style="margin-left: 40px;"></span>
                <span>G</span>
                <span>A</span>
                <span>M</span>
                <span>E</span>
            </h1>
        </div>

        <div id="select">
            <ul>
<!-----------------------------面倒なので　序章は飛ぶ-----InputName------Town----------------------------------------->
                <li><a href="InputName">GAME START</a></li>
                <li><a href="Countinue">COUNTINUE</a></li>
                <li><a href="#">SET UP</a></li>
            </ul>
        </div>

        </section>
    </main>
    <script>
        gsap.from('.left_over, .right_over', {
            duration: .8,
            bottom: '100%',
            onComplete:function(){
                gsap.to('.left_over, right_over', {
                    duration: .8,
                    top:'100%',
                    delay: .2,
                    stagger: .2
                })
            }
        })

        gsap.from('#left-bg, #right_bg', {
            duration: .3,
            scale: 1.3,
            delay:1.3
        })

        gsap.from('#content', {
            duration: .5,
            botto:0,
            opacity:0,
            delay:2
        })

        gsap.from('#select li', {
            duration: .5,
            botto:'4%',
            opacity:0,
            delay:2.2
        })

        gsap.from('span', {
            duration: .3,
            fontSize:2,
            top:50,
            opacity:0,
            delay:1.8,
            stagger: .1
        })

    </script>
</body>
</html>