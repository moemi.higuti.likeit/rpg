<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" >
    <title>教会</title>
    <link rel="stylesheet" href="css/church.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/monster.css">
    <link rel="stylesheet" href="css/item.css">
    <script src="jquery.min.js"></script>
    <style>
        *{
            /*color:red;
            */
        }
        .paramater,
        .comand,
        .texts{
        /*
            border: 3px solid red !important;*/
        }

        body{
            background:url(img/church.jpg);
            background-repeat: no-repeat;
            background-size: cover;
        }
    </style>
</head>
<body>
    <main>
        <section>
            <div class="paramater">
                <ul>
                    <li>名前　　　：${hero.name}</li>
                    <li>HP　　　 ：　${hero.hp}　</li>
                    <li>レベル　　：　${hero.lv}</li>
                    <li>ゴールド　：　${hero.gold}　G</li>
                </ul>
            </div>
            <div class="control col-md-10 row">
                <div class="col-md-3 comand">
                    <ul>
                    <c:if test="${FinishMessage == null}">
                        <li id="goout">お祈りする</li>
                        <a href="Finish"><li>冒険をやめる</li></a>
                        <a href="Town"><li>街中にもどる</li></a>
                        <li>神父と話す</li>
                    </c:if>
                    </ul>
                </div>
                <div class="col-md-9 texts">
                    <p id="a"><c:if test="${ErroeMessage != null}">
		                     		${ErroeMessage}

						</c:if>

						<c:if test="${FatherMessage != null}">
		                     		${FatherMessage}
						</c:if>
						<c:if test="${FinishMessage != null}">
		                     		${FinishMessage}
		                     		<form action="Finish" method="POST">
			                     		<button type="submit" name="action">
					                        <img src="img/arrow.jpg" alt="">
					                    </button>

						</c:if>
					</p>
                    <button>
                        <img src="img/arrow.jpg" alt="">
                    </button>
                    </form>
                </div>
            </div>
        </section>

<!--ーーーーーーーーーーーお祈りのモデルーーー  -->
<section>
    <div class="goout itemcollections">
        <div class="itemselect">
            <ul>

                <button>
                    <li class="show row">
                        <div class="text mx-2 row">
                            <p class="mx-2" >ぼうけんの書を作る</p>
                        </div>
                    </li>
                </button>

                <button>
                    <li id="status" class="row">
                        <div class="text mx-2 row">
                            <p class="mx-2" >ぼうけんの書を上書きする</p>
                        </div>
                    </li>
                </button>

                <a href="Password">
                    <li class="row">
                        <div class="text mx-2 row">
                            <p class="mx-2" >復活の呪文を聞く</p>
                        </div>
                    </li>
                </a>

                <a href="Revelation">
                    <li class="row">
                        <div class="text mx-2 row">
                            <p class="mx-2" >お告げを聞く</p>
                        </div>
                    </li>
                </a>

            </ul>

            <div class="back">
                <p class="goout">やめる</p>
            </div>
        </div>
    </div>
</section>



    <!--ーーーーーーーーーーーーーー  ぼうけんの書の作成最終確認を選ぶセクション　ーーーーーーーーー-->
    <form action="MakeStory" method="POST">

    <section>
        <div class="intown itemcollections">
            <div class="itemselect">

                    <div class="text">
                        <p class="mx-2" >ぼうけんの書を作成を実行してよろしいでしょうか</p>

                    <ul class="savedate row">
                        <li class="mr-1">
                            <button type="submit"  name="action">
                                <p>はい</p>
                            </button>
                        </li>
                        <li class="ml-1"><p class="disapear">やめる</p></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

<!--ーーーーーーーーーーーぼうけんの書のパスワードセクションーーーーーーーーーーーー-->
    <section class="savedatesection itemcollections">

        <div >
            <div class="itemselect  col-md-6">
                <div class="status">
                    <div class="text">
                        <p class="mx-2" >復活の呪文をつくってください</p>
                        <li class="update"><input name="savePassword" type="text"></li>
                    <ul class="savedate row">

                        <li class="mr-1">
                            <p class="lastsave">はい</p>
                        </li>
                        <li class="ml-1"><p class="disapear">やめる</p></li>
                    </ul>
                </div>

            </div>
        </div>
    </section>
     </form>

    <!--ーーーーーーーーーーーーーー  ぼうけんの書の上書きの最終確認を選ぶセクション　ーーーーーーーーー-->
    <form action="Update" method="POST">
    <section>
        <div class="updatefinalform itemcollections">
            <div class="itemselect">

                    <div class="text">
                        <p class="mx-2" >ぼうけんの書の上書きをしてよろしいでしょうか</p>

                    <ul class="savedate row">
                        <li class="mr-1">
                            <button type="submit"  name="action">
                                <p>はい</p>
                            </button>
                        </li>
                        <li class="ml-1"><p class="disapear">やめる</p></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>


    <!--ーーーーーーーーーーーぼうけんの書上書きの時を作る　時のパスワードーーーーーーーーーーーー-->
    <section class="updateform itemcollections">

        <div >
            <div class="itemselect  col-md-6">
                <div class="status">
                    <div class="text">
                        <p class="mx-2" >上書きする冒険の書の復活の呪文を教えてください</p>
                        <li class="update"><input name="savePassword" type="text"></li>
                    <ul class="savedate row">

                        <li class="mr-1">
                            <button>
                                <p class="updateask">はい</p>
                            </button>
                        </li>
                        <li class="ml-1"><p class="disapear">やめる</p></li>
                    </ul>
                </div>

            </div>
        </div>
    </section>
    </form>


</main>
    <script src="common.js"></script>
    <script>

         //お祈りを押したときに出るものの処理
         $(function(){
            $('#goout').click(function(){
            $('.goout').fadeIn();
            });
            $('.goout').click(function(){
            $('.goout').fadeOut();
            });
        })

        //ぼうけんの書の上書きセクションを見るときの画面の処理
        $(function(){
            $('.show').click(function(){
            $('.savedatesection').fadeIn();
            });
            $('.disapear').click(function(){
            $('.savedatesection').fadeOut();
            });
        })

        //ぼうけんの書を作るで実行するか聞くとき
        $(function(){
            $('.lastsave').click(function(){
            $('.intown').fadeIn();
            });
            $('.lastsave').click(function(){
                $('.savedatesection').fadeOut();
            });
            $('.disapear').click(function(){
            $('.intown').fadeOut();
            });
        })

        //ぼうけんの書を上書きするか聞くとき
        $(function(){
            $('.updateask').click(function(){
            $('.updatefinalform').fadeIn();
            });
            $('.updateask').click(function(){
                $('.updateform').fadeOut();
            });
            $('.disapear').click(function(){
            $('.updatefinalform').fadeOut();
            });
        })

        //ぼうけんの書を作る　時のパスワード
        $(function(){
            $('#status').click(function(){
            $('.updateform').fadeIn();
            });
            $('.disapear').click(function(){
            $('.updateform').fadeOut();
            });
        })



    </script>
</body>
</html>