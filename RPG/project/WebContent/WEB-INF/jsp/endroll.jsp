<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>エンドロール</title>
    <script src="jquery.min.js"></script>
    <script type="text/javascript" src="static/endcredits.js"></script>

    <style>
        @import url(https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300);
* {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}

html, body {
  height: 100%;
}

body {
  background: radial-gradient(ellipse at top left, #334455 0%, #112233 100%);
  overflow: hidden;
}

.wrapper {
  position: absolute;
  top: 100%;
  left: 50%;
  width: 450px;
  margin-left: -200px;
  font: 300 30px/1 'Open Sans Condensed', sans-serif;
  text-align: center;
  text-transform: uppercase;
  color: #fff;
  animation: 60s credits linear infinite;
}

.movie {
  margin-bottom: 50px;
  font-size: 60px;
  letter-spacing: 5px;
}

.job {
  margin-bottom: 5px;
  font-size: 18px;
}

.name {
  margin-bottom: 50px;
  font-size: 35px;
}
a{
    text-decoration: none;
    color: white;
}
img{
    margin: auto;
    width: 450px;
}

@keyframes credits {
  0% {
    top: 100%;
  }

  100% {
    top: -500%;
  }
}
    </style>
</head>
<body>
    <a href="Save"> <div class='wrapper'>
        <div class='movie'>RPG GAME</div>
        <div class='job'>制作者 スタッフ</div>
        <div class='name'>HIGUCHI</div>
        <div class='job'>シナリオ</div>
        <div class='name'>FF1 より引用</div>
        <div class='job'>キャラクターデザイン</div>
        <div class='name'>プロの方々　主にドラクエ</div>
        <div class='job'>アイテムデザイン</div>
        <div class='name'>プロの方々　主にドラクエ</div>
        <div class='job'>音響担当</div>
        <div class='name'>NOBUDY</div>
        <div class='job'>シナリオ　アシスタント</div>
        <div class='name'>HIGUCHI</div>
        <div class='job'>チーフ　プログラマー</div>
        <div class='name'>higuchi & higuchi</div>
        <div class='job'>アシスタント　プログラマー</div>
        <div class='name'>誰もいない。。。</div>
        <div class='job'>グラフィックデザイン</div>
        <div class='name'>そこら辺のページから勝手に使いました。</div>
        <div class='job'>ASSISTANT</div>
        <div class='name'>HIGUCHI</div>
        <div class='job'>screenplay by</div>
        <div class='job'>ASSISTANT</div>
        <div class='name'>HIGUCHI</div>
        <div class='name'>HIGUCHI</div>
        <div class='job'>ASSISTANT</div>
        <div class='name'>HIGUCHI</div>
        <div class='job'>ASSISTANT</div>
        <div class='name'>HIGUCHI</div>
        <div class='job'>DIRECTOR</div>
        <div class='name'>ME</div>
        <div class='job'>PRODUCER</div>
        <div class='job'>ASSISTANT</div>
        <div class='name'>HIGUCHI</div>
        <div class='job'>ASSISTANT</div>
        <div class='name'>HIGUCHI</div>
        <div class='job'>DIRECTOR</div>
        <div class='name'>ME</div>
        <div class='job'>PRODUCER</div>
        <div class='name'>NO BUDY</div>
        <div class='job'>Mentor</div>
        <div class='name'>Mr. Wada</div>
        <div class='job'>Mentor</div>
        <div class='name'>Mrs. Shirasawa</div>
        <div class='job'>Mentor</div>
        <div class='name'>Mr. Tanuma</div>
        <div class='job'>ご協力ありがとうございます</div>
        <div class='name'>おかげで作れました</div>

        <img src="img/thank.png" alt="">
      </div>
</a>
</body>
</html>