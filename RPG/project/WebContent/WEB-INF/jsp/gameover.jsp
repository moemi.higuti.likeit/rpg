<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" >
    <title>ゲームオーバー</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/over.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.2.6/gsap.min.js"></script>
    <link rel="stylesheet" href="css/gotoshop.css">
</head>
<body>
    <main>
        <section>
           <div class="gameover">
              <a href="Opening"><img src="img/gameover.jpg"></a>
           </div>
        </section>
    </main>
    <script>
        gsap.from('.gameover', {
        duration: .8,
        top:50,
        opacity:0,
        delay:1.8,
        stagger: .1
    })
    </script>
</body>
</html>