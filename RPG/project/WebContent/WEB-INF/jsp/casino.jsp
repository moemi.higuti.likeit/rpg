<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" >
    <title>カジノ</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/monster.css">
    <link rel="stylesheet" href="css/item.css">
    <script src="jquery.min.js"></script>
    <style>
        body{
            background:url(img/casino02.jpg);
            background-repeat: no-repeat;
            background-size: cover;
        }
    </style>
</head>
<body>
    <main>
        <section>

            <div class="paramater">
                <ul>
                    <li>名前　　　：${hero.name}</li>
                    <li>HP　　　 ：　${hero.hp}　</li>
                    <li>レベル　　：　${hero.lv}</li>
                    <li>ゴールド　：　${hero.gold}　G</li>
                </ul>
            </div>

            <div class="control col-md-10 row">
                <div class="col-md-3 comand">
                    <ul>
                        <li id="showbuy" >遊ぶ</li>
                        <li id="showsell">隠しボスとたたかう</li>
                        <a href="Town"><li>街中に戻る</li></a>
                        <li id="nomeaning">バニーと話す</li>
                    </ul>
                </div>
                <div class="col-md-9 texts">
                    <p class="talker">バニー : </p>
                    <p id="a">「　いらっしゃいませ！　楽しんでいってください!!!」</p>
                    <button>
                        <img src="img/arrow.jpg" alt="">
                    </button>
                </div>
            </div>
        </section>

        <section>
            <div class="itembuy itemcollections">
                <div class="itemselect">
                    <ul>

                        <a href="CasinoSlot">
                            <li class="row">
                                <img class="mr-3" src="img/slot.jpg" alt="">
                                <div class="text mx-2 row">
                                    <p class="mx-2" >スロット</p>
                                </div>
                            </li>
                        </a>

                        <a href="Shinkei">
                            <li class="row">
                                <img class="mr-3" src="img/shinkei.jpg" alt="">
                                <div class="text mx-2 row">
                                    <p class="mx-2" >神経衰弱</p>
                                </div>
                            </li>
                        </a>

                        <a href="CasinoHae">
                            <li class="row">
                                <img class="mr-3" src="img/hae.png" alt="">
                                <div class="text mx-2 row">
                                    <p class="mx-2" >ハエたたき</p>
                                </div>
                            </li>
                        </a>

                        <a href="Tetorisu">
                            <li class="row">
                                <img class="mr-3" src="img/tetorisu.jpg" alt="">
                                <div class="text mx-2 row">
                                    <p class="mx-2" >テトリス</p>
                                </div>
                            </li>
                        </a>

                    </ul>

                    <div class="back">
                        <p class="showdown">遊ぶのをやめる</p>
                    </div>
                </div>
            </div>
        </section>

        <!-- 道具を売るリスト-->
        <section>
            <div class="itemsell itemcollections">
                <div class="itemselect">
                    <ul>

                        <button>
                            <li class="row">

                                <div class="text mx-2 row">
                                    <p class="mx-2" >隠しボスは本当に強いです。戦いますか?</p>
                                </div>
                            </li>
                        </button>

                    </ul>

                    <div class="back">
                        <p class="showdown">やめる</p>
                    </div>
                </div>
            </div>
        </section>

    </main>
    <script src="common.js"></script>
    <script>
        //アイテムを買うときの画面の処理
        $(function(){
            $('#showbuy').click(function(){
            $('.itembuy').fadeIn();
            });
            $('.showdown').click(function(){
            $('.itembuy').fadeOut();
            });
        })

         //アイテムを売るときの画面の処理
        $(function(){
            $('#showsell').click(function(){
            $('.itemsell').fadeIn();
            });
            $('.showdown').click(function(){
            $('.itemsell').fadeOut();
            });
        })
    </script>
</body>
</html>