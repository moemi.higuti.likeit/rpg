<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" >
    <title>宿屋</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/monster.css">
    <link rel="stylesheet" href="css/item.css">
    <link rel="stylesheet" href="css/gotoshop.css">
    <script src="jquery.min.js"></script>
    <Style>
        body{
            background:url(img/yadoya01.jpg);
            background-repeat: no-repeat;
            background-size: cover;
        }
    </Style>
</head>
<body>
    <main>
        <section>

            <div class="paramater">
                <ul>
                   <li>名前　　　：　${hero.name}</li>
                   <li>HP　　　 ：　${hero.hp}　</li>
                   <li>レベル　　：　${hero.lv}</li>
                   <li>ゴールド　：　${hero.gold}　G</li>
               </ul>
            </div>

            <div class="control col-md-10 row">
                <div class="col-md-3 comand">
                    <ul>
                     <c:if test="${RestMessage == null}">
	                   		<li id="showbuy">宿屋に泊まる</li>
                        	<a href="Town"><li>街中に戻る</li></a>
                        	<button><li>店主と話す</li></button>
                     </c:if>
                     </ul>
                </div>
                <div class="col-md-9 texts">
                    <p class="talker">宿屋の店主 : </p>
                    <c:if test="${RestMessage == null}">
                   		<p id="a">「　いらっしゃいませ！ ご用件は何でしょうか？　」</p>
                   		<br>
					</c:if>


	                    <c:if test="${RestMessage != null}">
	                   		${RestMessage}<br><br><br>
	                    <button>
	                        <a href = "Town"><img src="img/arrow.jpg" alt=""></a>
	                    </button>
	                    </c:if>

                </div>
            </div>
        </section>

        <section>
            <div class="itemcollections">
                <div class="itemselect">
                    <ul>

                        <form action="Inn" method="POST">

                        	<button type="submit" name="action">
                            	<li class="row">
	                                <img class="mr-3" src="img/yadoya02.jpg" alt="">
	                                <div class="text mx-2 row">
	                                    <p class="mx-2" >泊まる</p>
	                                    <span class="mx-3" >100G</span>
	                                    <input type="hidden" name="yes" value="yes">
	                                </div>
                            	</li>
                        	</button>
							</form>

                    </ul>

                    <div class="back">
                        <p class="showdown">やめる</p>
                    </div>
                </div>
            </div>
        </section>

    </main>

    <script src="common.js"></script>
    <script>
        $(function(){
            $('#showbuy').click(function(){
            $('.itemcollections').fadeIn();
            });
            $('.showdown').click(function(){
            $('.itemcollections').fadeOut();
            });
        })

    </script>
</body>
</html>