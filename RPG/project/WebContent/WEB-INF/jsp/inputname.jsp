<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" >
    <title>序章2</title>

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/zyosyou.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.2.6/gsap.min.js"></script>
    <link rel="stylesheet" href="css/gotoshop.css">
</head>
<body>
    <main>
        <section>
        <form action="InputName" method="POST">
           <div class="comand gamestarting col-md-6">
               <p id="a">あなたのお名前は何ですか？
               </p>
               <c:if test="${ErroeMessage != null}">
               <p>${ErroeMessage}</p>

               </c:if>
               <input type="text" name="name">
               <button>
	               <img class="next" src="img/arrow.jpg" alt="">
               </button>
               <div class="goback my-2">
                <a href="Opening">オープニング画面にもどる</a>
            </div>
           </div>

		</form>
        </section>
    </main>

    <script src="common.js"></script>
    <script>
        gsap.from('.gameclear', {
        duration: 1.2,
        opacity:0,
        delay:2.8,
        stagger: .1
    })
    </script>
</body>
</html>