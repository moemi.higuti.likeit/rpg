<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" >
    <title>強制セーブ</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/clear.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.2.6/gsap.min.js"></script>
    <link rel="stylesheet" href="css/gotoshop.css">
    <link rel="stylesheet" href="css/item.css">
    <link rel="stylesheet" href="css/church.css">
</head>
<body>
    <main>

        <section>
        <form action="Save" method="POST">
          <div class="goout">
              <div class="itemselect col-md-6">

						<c:if test="${ErroeMessage != null}">
		                     		${ErroeMessage}
						</c:if>
                      <div class="text">
                        <p id="a">
						<c:if test="${ErroeMessage == null}">お疲れ様です
                          <br><p id="b"></h2>ぼうけんの書を新しく作成します
                          <br><p id="c"></h2>復活の呪文をつくってください
                          </c:if>
                      </div>
                      <div class="update">
                        <input name="savePassword"  class="mt-2 aftergameclear" type="text">
                        <input type="hidden" name="finish" value="finish">
                        <button >
                          <img sizes="30px" class="m-2 aftergameclear" src="img/arrow.jpg" alt="">
                      </button>
                      </div>
              </div>
          </div>
           </form>
      </section>


    </main>

    <script>
    /* ----- option ----- */
var id = ['a','b','c']; //指定するidを全て配列で渡す
var txSp = 100; // テキストの表示速度
var dly = 1000; // 次の文章までの待ち時間
/* ----- option ----- */
var count = 0;
var tx = [];
var txCount = [];

window.onload = function(){
  kamikakushi();
  countSet();
  itimozi();
}

function countSet(){ // 文字数カウントの初期設定
  for(n=0;n<id.length;n++){
    txCount[n] = 0;
  }
}

function kamikakushi(){ // 要素をtx[i]に保持させ、非表示にする
  for(i=0;i<id.length;i++){
    id[i] = document.getElementById(id[i]);
    tx[i] = id[i].firstChild.nodeValue; // 初期の文字列
    id[i].innerHTML = '';
  }
}

function itimozi(){ //　一文字ずつ表示させる
  id[count].innerHTML = tx[count].substr( 0, ++txCount[count] )+"_"; // テキストの指定した数の間の要素を表示
  if(tx[count].length != txCount[count]){ // Count が初期の文字列の文字数と同じになるまでループ
    setTimeout("itimozi()",txSp); // 次の文字へ進む
  }else{
  id[count].innerHTML = tx[count].substr( 0, ++txCount[count] ); // テキストの指定した数の間の要素を表示
    count++; // 次の段落に進む為のカウントアップ
    if(count != id.length){ // id数が最後なら終了
    setTimeout("itimozi()",dly); // 次の段落へ進む
    }
  }
}
</script>

    <script>
        gsap.from('.aftergameclear', {
        duration: 1.2,
        opacity:0,
        delay:12.8,
        stagger: .1
    })
    </script>
</body>
</html>