
.st-kaiwa-hukidashi,
.st-kaiwa-hukidashi2 {
	font-size: 17px;
	line-height: 28px;
}

/* 会話レイアウト */

.st-kaiwa-box {
	width: 100%;
	height: auto;
	margin-bottom: 20px;
	display: table;
}

.st-kaiwa-face {
	text-align: center;
	display: table-cell;
	width: 60px;
	vertical-align: middle;
}

.st-kaiwa-face img {
	border-radius: 60px;
	border: 1px solid #ccc;
	margin-bottom: 5px;
}

.st-kaiwa-face-name {
	color: #616161;
	font-size: 70%;
	line-height: 1.5;
	max-width: 60px;
}

.st-kaiwa-area {
	display: table-cell;
	margin: 0;
	vertical-align: middle;
	text-align: left;
}

.st-kaiwa-hukidashi {
	display: inline-block;
	padding: 15px 20px;
	margin-left: 20px;
	border-width: 1px;
	border-style: solid;
	border-color: transparent;
	border-radius: 7px;
	position: relative;
	background-color: #f9f9f9;
}

.post .st-kaiwa-hukidashi p:last-child {
	margin-bottom: 0;
}

.st-kaiwa-hukidashi::after {
	content: "";
	position: absolute;
	top: 50%;
	left: -10px;
	margin-top: -10px;
	display: block;
	width: 0;
	height: 0;
	border-style: solid;
	border-width: 10px 10px 10px 0;
	border-color: transparent #f9f9f9 transparent transparent;
}

/*ふきだし反対*/

.st-kaiwa-face2 {
	text-align: center;
	display: table-cell;
	width: 60px;
	vertical-align: middle;
}

.st-kaiwa-face2 img {
	border-radius: 60px;
	border: 1px solid #ccc;
	margin-bottom: 5px;
}

.st-kaiwa-face-name2 {
	color: #616161;
	font-size: 70%;
	line-height: 1.5;
	max-width: 60px;
}

.st-kaiwa-area2 {
	display: table-cell;
	margin: 0;
	vertical-align: middle;
	text-align: right;
}

.st-kaiwa-hukidashi2 {
	display: inline-block;
	padding: 15px 20px;
	margin-right: 20px;
	border-width: 1px;
	border-style: solid;
	border-color: transparent;
	border-radius: 7px;
	position: relative;
	background-color: #f9f9f9;
	text-align: left;
}

.post .st-kaiwa-hukidashi2 p:last-child {
	margin-bottom: 0;
}

.st-kaiwa-hukidashi2::after {
	content: "";
	position: absolute;
	top: 50%;
	right: -10px;
	margin-top: -10px;
	display: block;
	width: 0;
	height: 0;
	border-style: solid;
	border-width: 10px 0 10px 10px;
	border-color: transparent transparent transparent #f9f9f9;
}

.st-kaiwa-hukidashi::before,
.st-kaiwa-hukidashi2::before {
	content: '';
	position: absolute;
	top: 50%;
	margin-top: -11px;
	display: block;
	width: 0;
	height: 0;
	border-style: solid;
	border-color: transparent;
	z-index: 0;
}

.st-kaiwa-hukidashi::before {
	left: -11px;
	border-width: 11px 11px 11px 0;
}

.st-kaiwa-hukidashi2::before {
	right: -11px;
	border-width: 11px 0 11px 11px;
}

@media only screen and (min-width: 600px) {
	.st-kaiwa-hukidashi,
	.st-kaiwa-hukidashi2 {
		font-size: 20px;
	}
}

@media print, screen and (min-width: 960px) {
	.st-kaiwa-hukidashi,
	.st-kaiwa-hukidashi2 {
		font-size: 15px;
		line-height: 25px;
	}
}


	
	
	
	
	
					
	
	
	
	
					
	
	
	
	
					
	
	
	
	
					
	
	
	
	
					
	
	
	
	
				