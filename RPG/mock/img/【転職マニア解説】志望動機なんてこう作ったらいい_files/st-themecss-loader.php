
/*グループ1
------------------------------------------------------------*/



/*縦一行目のセル*/
table tr td:first-child {
			}

/*横一行目のセル*/
table tr:first-child {
			}

/* 会話レイアウト */

/*この記事を書いた人*/
#st-tab-menu li.active {
  background: #039BE5;
}
#st-tab-box {
	border-color: #039BE5;
}
.post #st-tab-box p.st-author-post {
	border-bottom-color: #039BE5;
}
.st-author-date{
	color:#039BE5;
}

/*TOC+*/
#toc_container {
	
	}



/*マル数字olタグ*/


.post .maruno ol li:before {
		}

/*チェックulタグ*/


.post .maruck ul li:before {
		}

/*Webアイコン*/





/*スライドショー矢印非表示*/

/*サイト上部のボーダー色*/

/*ヘッダーの背景色*/

		#headbox {
							background-color: transparent;
							
					background: none;
										}

		

/*サイトの背景色*/

/*ヘッダー下からの背景色*/
#content-w {
                   
	}

/*メインコンテンツのテキスト色*/
.post > * {
	color: ;
}

input, textarea {
	color: #000;
}

/*メインコンテンツのリンク色*/

a, 
.no-thumbitiran h3:not(.st-css-no) a, 
.no-thumbitiran h5:not(.st-css-no) a {
	color: ;
}

/*サイドのテキスト色*/
#side aside > *,
#side li.cat-item a,
#side aside .kanren .clearfix dd h5:not(.st-css-no) a,
#side aside .kanren .clearfix dd p {
	color: ;
}

/*サイドバーウィジェットの背景色*/

/*メインコンテンツの背景色*/
main {
	background: #ffffff;
}

/*メイン背景色の透過*/


/*ブログタイトル*/

header .sitename a {
	color: #dd3333;
}

/* メニュー */
nav li a {
	color: #dd3333;
}

/*ページトップ*/
#page-top a {
	background: #039BE5;
}



/*キャプション */

header h1 {
	color: #dd3333;
}

header .descr {
	color: #dd3333;
}

/* アコーディオン */
#s-navi dt.trigger .op {
	background: ;
	color: #13b0fc;
}



/*アコーディオンメニュー内背景色*/
#s-navi dd.acordion_tree {
	}

/*追加ボタン1*/
#s-navi dt.trigger .op-st {
				color: #13b0fc;
	}

/*追加ボタン2*/
#s-navi dt.trigger .op-st2 {
				color: #13b0fc;
	}

.acordion_tree li a {
	color: #dd3333;
}


/*スマホフッターメニュー*/
#st-footermenubox a {
	color: #000; 
}


/* スマホメニュー文字 */
.acordion_tree ul.menu li a, 
.acordion_tree ul.menu li {
	color: #000000;
}

	.acordion_tree ul.menu li {
		border-bottom-color: #000000;
	}

/*グループ2
------------------------------------------------------------*/
/*Webフォント*/

/* 投稿日時・ぱんくず・タグ */
#breadcrumb,
#breadcrumb div a,
div#breadcrumb a,
.blogbox p,
.tagst,
#breadcrumb ol li a,
#breadcrumb ol li h1,
#breadcrumb ol li,
.kanren:not(.st-cardbox) .clearfix dd .blog_info p,
.kanren:not(.st-cardbox) .clearfix dd .blog_info p a
{
	color: #616161;
}

/* 記事タイトル */
	.entry-title:not(.st-css-no),
	.post .entry-title:not(.st-css-no) {
		color: #000000;
					background-color: transparent;
						
				background: none;
						}



	.entry-title:not(.st-css-no),
	.post .entry-title:not(.st-css-no) {
		padding-top:5px;
		padding-bottom:5px;
	}






	.entry-title:not(.st-css-no),
	.post .entry-title:not(.st-css-no) {
		padding-top:5px!important;
		padding-bottom:5px!important;
	}


/* h2 */




	            h2:not(.st-css-no) {
                background: #0270d1;
                color: #ffffff;
                position: relative;
                border: none;
                margin-bottom:30px;
					padding-left:20px!important;
		
					padding-top:10px!important;
			padding-bottom:10px!important;
		            }
        
            h2:not(.st-css-no):after {
                border-top: 10px solid #0270d1;
                content: '';
                position: absolute;
                border-right: 10px solid transparent;
                border-left: 10px solid transparent;
                bottom: -10px;
                left: 30px;
                border-radius: 2px;
            }
        
            h2:not(.st-css-no):before {
                border-top: 10px solid #0270d1;
                content: '';
                position: absolute;
                border-right: 10px solid transparent;
                border-left: 10px solid transparent;
                bottom: -10px;
                left: 30px;
            }
        
       		        
	


/* h3 */



	.post h3:not(.st-css-no):not(.st-matome):not(.rankh3):not(#reply-title) {
		position: relative;
		padding-left:0;
		padding-bottom: 10px;
		border-top:none;
		border-bottom-width:3px;
					border-bottom-color: #1e73be!important;
							padding-left:20px!important;
		
					padding-top:10px!important;
			padding-bottom:10px!important;
		                color: #020042;
                background-color:transparent;
	}

	.post h3:not(.st-css-no):not(.st-matome):not(.rankh3):not(#reply-title)::after {
		position: absolute;
		bottom: -3px;
		left: 0;
		z-index: 3;
		content: '';
		width: 30%;
		height: 3px;
		                	background-color: #2356e0;
			}




/*h4*/


    .post h4:not(.st-css-no):not(.st-matome):not(.rankh4):not(.point) {
                    border-left: 5px solid ;
                color: #026cc9;
                    background-color: #ffffff;
            
            
            
                    padding-left:20px;
            
                    padding-top:10px;
            padding-bottom:10px;
            
            }


/*まとめ*/



    .post .st-matome:not(.st-css-no):not(.rankh4):not(.point) {
                color: #000000;
                    background-color: transparent;
            
            
            
                    padding-left:20px!important;
            
                    padding-top:10px!important;
            padding-bottom:10px!important;
            
            }


/* サイド見出し */
aside h4:not(.st-css-no),
#side aside h4:not(.st-css-no),
.st-widgets-title {
	font-weight:bold;
	color: #039BE5;
}

/*h5*/


    .post h5:not(.st-css-no):not(.st-matome):not(.rankh5):not(.point):not(.st-cardbox-t):not(.popular-t):not(.kanren-t):not(.popular-t) {
				color: #039BE5;
					background-color: transparent;
			
			
					border-bottom : solid 1px ;
			
					padding-left:20px!important;
			
					padding-top:10px!important;
			padding-bottom:10px!important;
			
			}


/* タグクラウド */
.tagcloud a {
	border-color: #039BE5;
	color: #039BE5;
}

/* NEW ENTRY & 関連記事 */
.post h4:not(.st-css-no):not(.rankh4).point, 
.cat-itiran p.point,
.n-entry-t {
	border-bottom-color: #039BE5;
}

.post h4:not(.st-css-no):not(.rankh4) .point-in, 
.cat-itiran p.point .point-in,
.n-entry {
	background-color: #039BE5;
	color: #ffffff;
}

/* カテゴリ */
.catname {
	background: #13b0fc;
	color:#ffffff;
}

.post .st-catgroup a {
	color: #ffffff;
}


/*グループ4
------------------------------------------------------------*/
/* RSSボタン */
.rssbox a {
	background-color: #039BE5;
}

/* SNSボタン */


.inyoumodoki, .post blockquote {
	background-color: #f3f3f3;
	border-left-color: #f3f3f3;
}

/*ブログカード
------------------------------------------------------------*/
/* 枠線 */

/* ラベル */
.st-cardbox-label-text {
		}

/*フリーボックスウィジェット
------------------------------------------------------------*/
/* ボックス */
.freebox {
	border-top-color: #039BE5;
	background: #fbfeff;
}

/* 見出し */
.p-entry-f {
	background: #039BE5;
	color: #ffffff;
}

/* エリア内テキスト */

/*メモボックス
------------------------------------------------------------*/

/*スライドボックス
------------------------------------------------------------*/

/*お知らせ
------------------------------------------------------------*/
/*お知らせバーの背景色*/
#topnews-box div.rss-bar {
			border-color: #13b0fc;
	}

#topnews-box div.rss-bar {
	color: #ffffff;

	/*Other Browser*/
	background: #039BE5;
	/*For Old WebKit*/
	background: -webkit-linear-gradient( #13b0fc 0%, #039BE5 100% );
	/*For Modern Browser*/
	background: linear-gradient( #13b0fc 0%, #039BE5 100% );
}

/*お知らせ日付の文字色*/
#topnews-box dt {
	color: #13b0fc;
}

#topnews-box div dl dd a {
	color: #000000;
}

#topnews-box dd {
	border-bottom-color: #13b0fc;
}

#topnews-box {
			background-color:transparent!important;
	}

/*追加カラー
------------------------------------------------------------*/
/*フッター*/
footer > *,
footer a,
#footer .copyr,  
#footer .copyr a, 
#footer .copy,  
#footer .copy a {
	}

footer .footermenust li {
	border-right-color:  !important;
}

/*フッター背景色*/

	#footer {
					background-color: transparent;
						
				background: none;
						            
           					max-width: 1030px; /*padding 15pxあり*/
			}

	

/*任意の人気記事
------------------------------------------------------------*/

.post .p-entry, #side .p-entry, .home-post .p-entry {
	background: #039BE5;
	color: #ffffff;
}

.pop-box, .nowhits .pop-box, .nowhits-eye .pop-box,
.st-eyecatch + .nowhits .pop-box {
	border-top-color: #039BE5;
	background: #fbfeff;
}

.pop-box:not(.st-wpp-views-widgets),
#side aside .kanren.pop-box:not(.st-wpp-views-widgets) {
			padding:20px 20px 10px;
	}

.pop-box:not(.st-wpp-views-widgets),
#side aside .kanren.pop-box:not(.st-wpp-views-widgets) {
		padding:30px 20px 10px;
		border: none;
}

.kanren.pop-box .clearfix dd h5:not(.st-css-no) a, 
.kanren.pop-box .clearfix dd p,
.kanren.pop-box .clearfix dd p a, 
.kanren.pop-box .clearfix dd p span, 
.kanren.pop-box .clearfix dd > *,
.kanren.pop-box h5:not(.st-css-no) a, 
.kanren.pop-box div p,
.kanren.pop-box div p a, 
.kanren.pop-box div p span, 
{
	color: !important;
}

	.poprank-no2,
	.poprank-no {
		background: #039BE5;
		color: #ffffff !important;
			}

/*WordPressPopularPosts連携*/

#st-magazine .st-wp-views, /*CARDs JET*/
#st-magazine .st-wp-views-limit, /*CARDs JET*/
.st-wppviews-label .wpp-views, /*Ex*/
.st-wppviews-label .wpp-views-limit, /*Ex*/
.st-wpp-views-widgets .st-wppviews-label .wpp-views {
			color: #ffffff;
				background:#039BE5;
	}

/*ウィジェット問合せボタン*/

.st-formbtn {
	
	
			/*Other Browser*/
		background: #039BE5;
	}

.st-formbtn .st-originalbtn-r {
	border-left-color: #ffffff;
}

a.st-formbtnlink {
	color: #ffffff;
}

/*ウィジェットオリジナルボタン*/

.st-originalbtn {
	
	
			/*Other Browser*/
		background: #039BE5;
	}

.st-originalbtn .st-originalbtn-r {
	border-left-color: #ffffff;
}

a.st-originallink {
	color: #ffffff;
}

/*ミドルメニュー（ヘッダーメニュー連動）
------------------------------------------------------------*/
.st-middle-menu {
			color: #ffffff;
				/*Other Browser*/
		background: #039BE5;
				border-top-color: #13b0fc;
		border-left-color: #13b0fc;
	}

.st-middle-menu .menu li a{
			color: #ffffff;
				border-bottom-color: #13b0fc;
		border-right-color: #13b0fc;
		}

/*固定ページサイドメニュー
------------------------------------------------------------*/
/*背景色*/
#sidebg {
	background: #fbfeff;

	}


	/*liタグの階層*/
	#side aside .st-pagelists ul li:not(.sub-menu) {
					border-top-color: #039BE5;
		
					border-left-color: #039BE5;
			border-right-color: #039BE5;
			}

	#side aside .st-pagelists ul .sub-menu li {
		border: none;
	}

	#side aside .st-pagelists ul li:last-child {
					border-bottom: 1px solid #039BE5;
			}

	#side aside .st-pagelists ul .sub-menu li:first-child {
					border-top: 1px solid #039BE5;
			}

	#side aside .st-pagelists ul li li:last-child {
		border: none;
	}

	#side aside .st-pagelists ul .sub-menu .sub-menu li {
		border: none;
	}
	
	#side aside .st-pagelists ul li a {
		color: #ffffff;
					/*Other Browser*/
			background: #039BE5;
			/* Android4.1 - 4.3 */
			background: url(""), -webkit-linear-gradient(top,  #13b0fc 0%,#039BE5 100%);

			/* IE10+, FF16+, Chrome26+ */
			background: url(""), linear-gradient(to bottom,  #13b0fc 0%,#039BE5 100%);
	
			}

	
	
	#side aside .st-pagelists .sub-menu a {
					border-bottom-color: #13b0fc;
				color: #039BE5;
	}

	#side aside .st-pagelists .sub-menu .sub-menu li:last-child {
		border-bottom: 1px solid #13b0fc;
	}

	#side aside .st-pagelists .sub-menu li .sub-menu a,
	#side aside .st-pagelists .sub-menu li .sub-menu .sub-menu li a {
		color: #039BE5;
	}



	#side aside .st-pagelists ul li a {
		padding-left:15px;
	}

	#side aside .st-pagelists ul li a {
		padding-top:8px;
		padding-bottom:8px;
	}

/*Webアイコン*/


/*コンタクトフォーム7送信ボタン*/
.wpcf7-submit {
	background: #039BE5;
	color: #ffffff;
}

/* メイン画像背景色 */


/*media Queries タブレットサイズ（959px以下）
----------------------------------------------------*/
@media only screen and (max-width: 959px) {

	/*-- ここまで --*/
}

/*media Queries タブレットサイズ以下
----------------------------------------------------*/
@media only screen and (min-width: 600px) {

}

/*media Queries タブレットサイズ（600px～959px）のみで適応したいCSS -タブレットのみ
---------------------------------------------------------------------------------------------------*/
@media only screen and (min-width: 600px) and (max-width: 959px) {

	
/*-- ここまで --*/
}


/*media Queries PCサイズ
----------------------------------------------------*/
@media only screen and (min-width: 960px) {

	
	
	
	/*ヘッダーの背景色*/
	
	/*メインコンテンツのボーダー*/
	
	
	/* メイン画像100% */
	

	/* スライドショー横並び */
	
	/*wrapperに背景がある場合*/
	
	/*メニュー*/
	#st-menuwide {
			border-top-color: #039BE5;
		border-bottom-color: #039BE5;
				border-left-color: #039BE5;
		border-right-color: #039BE5;
	
			/*Other Browser*/
		background: #039BE5;
		/* Android4.1 - 4.3 */
		background: url(""), -webkit-linear-gradient(top,  #13b0fc 0%,#039BE5 100%);

		/* IE10+, FF16+, Chrome26+ */
		background: url(""), linear-gradient(to bottom,  #13b0fc 0%,#039BE5 100%);
		}

	

	header .smanone ul.menu li, 
	header nav.st5 ul.menu  li,
	header nav.st5 ul.menu  li,
	header #st-menuwide div.menu li,
	header #st-menuwide nav.menu li
	{
			border-right-color: #13b0fc;
		}

	header .smanone ul.menu li, 
	header nav.st5 ul.menu  li,
	header #st-menuwide div.menu li,
	header #st-menuwide nav.menu li {
		border-right-color: #13b0fc;
	}

	header .smanone ul.menu li a, 
	header nav.st5 ul.menu  li a,
	header #st-menuwide div.menu li a,
	header #st-menuwide nav.menu li a {
		color: #ffffff;
	}

	
	header .smanone ul.menu li li a {
		background: #13b0fc;
		border-top-color: #039BE5;

	}

	/*メニューの上下のパディング*/
		

	/* グローバルメニュー100% */
	
	/*ヘッダーウィジェット*/
	header .headbox .textwidget {
		background: #fbfeff;
		color: #000000;
	}

	/*ヘッダーの電話番号とリンク色*/
	.head-telno a, #header-r .footermenust a {
		color: #000000;
	}

	#header-r .footermenust li {
		border-right-color: #000000;
	}

	/*トップ用おすすめタイトル*/
	.nowhits .pop-box {
		border-top-color: #039BE5;
	}

	/*記事エリアを広げる*/
	
	/*記事タイトル*/
			.entry-title:not(.st-css-no),
		.post .entry-title:not(.st-css-no) {
			color: #000000;
										
					}
	
/*-- ここまで --*/
}
