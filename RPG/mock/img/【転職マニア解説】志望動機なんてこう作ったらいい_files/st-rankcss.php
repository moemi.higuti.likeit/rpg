
@charset "UTF-8";
/*----------------------------------
ランク
-----------------------------------*/

img[width="1"] {
	position: absolute;
	overflow: hidden;
	clip: rect(0 0 0 0);
	-webkit-clip-path: inset(50%);
	clip-path: inset(50%);
	margin: -1px;
	padding: 0;
	width: 1px;
	height: 1px;
	border: 0;
	white-space: nowrap;
}

.scroll-box img[width="1"] {
	position:static;
}


.rankst-wrap {
	margin-bottom: 10px;
}

.rankst {
	margin-bottom: 10px;
	overflow: hidden;
}

.rankst-box {
	margin-bottom:0px;
}

.rankst p {
	margin-bottom: 10px;
	overflow: hidden;
}

.rankst-cont blockquote {
	background-color: transparent;
	background-image: none;
	padding:0px;
	margin-top: 0px;
	border: none;
}

.rankst-cont {
	margin: 0px;
}

.rankst-l.post, /*ランキング*/
.rankst-l /*ランキングプラグイン*/ 
{
	text-align:center;
	padding:0 0 20px;
}

.rankstlink-l {
	width: 100%;
	text-align: center;
}

.rankstlink-r {
	float: right;
	width: 100%;
}

/*スター*/

.st-star {
	color:#FFB400;
	font-size:15px;
}

/*詳細ページへのリンクボタン*/
.rankstlink-l p a {
	font-family: Helvetica , "游ゴシック" , "Yu Gothic" , sans-serif;
	display: block;
	width: 100%;
	box-sizing:border-box;
	text-align: center;
	padding: 10px;
	background: #039BE6;
	color: #ffffff;
	text-decoration: none;
	margin-right: auto;
	margin-left: auto;
	border-radius: 5px;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	font-weight:bold;
}

.rankstlink-l p {
	width: 90%;
	text-align: center;
	margin-right: auto;
	margin-left: auto;
	border-radius: 5px;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	font-weight:bold;
	box-shadow: 0 2px 0 #02689a;
	position:relative;
}

.rankstlink-l p:hover {
	box-shadow: 0 1px 0 #02689a;
	top:1px;
}

/*投稿用詳細ページリンクボタン*/
.rankstlink-l2 p a {
	display: block;
	width: 100%;
	box-sizing:border-box;
	text-align: center;
	padding: 10px;
	background: #039BE6;
	color: #ffffff;
	text-decoration: none;
	margin-right: auto;
	margin-left: auto;
	border-radius: 5px;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	font-weight:bold;
	box-sizing:border-box;
}

.rankstlink-l2 p {
	width: 90%;
	text-align: center;
	margin-right: auto;
	margin-left: auto;
	border-radius: 5px;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	font-weight:bold;
	box-shadow: 0 2px 0 #02689a;
	position:relative;
}

.rankstlink-l2 p:hover {
	box-shadow: 0 1px 0 #02689a;
	top:1px;
}


/*詳細ページのみ*/

.rankstlink-b p a {
	font-family: Helvetica , "游ゴシック" , "Yu Gothic" , sans-serif;
	display: block;
	width: 100%;
	box-sizing:border-box;
	text-align: center;
	padding: 10px;
	background: #039BE6;
	color: #ffffff;
	text-decoration: none;
	margin-right: auto;
	margin-left: auto;
	border-radius: 5px;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	font-weight:bold;
}

.rankstlink-b p {
	width: 90%;
	text-align: center;
	margin-right: auto;
	margin-left: auto;
	border-radius: 5px;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	font-weight:bold;
	box-shadow: 0 2px 0 #02689a;
	position:relative;
}

.rankstlink-b p:hover {
	box-shadow: 0 1px 0 #02689a;
	top:1px;
}

/*アフィリエイトのリンクボタン*/
.rankstlink-r p a {
	font-family: Helvetica , "游ゴシック" , "Yu Gothic" , sans-serif;
	display: block;
	width: 100%;
	box-sizing:border-box;
	text-align: center;
	padding: 10px;
	background-color: #E53935;
	color: #ffffff;
	text-decoration: none;
	margin-right: auto;
	margin-left: auto;
	border-radius: 5px;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	font-weight:bold;
}

.rankstlink-r p {
	width: 90%;
	margin-right: auto;
	margin-left: auto;
	border-radius: 5px;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	box-shadow: 0 2px 0 #b61b17;
	position:relative;
}

.rankstlink-r p img{
	display:none;
}

.rankstlink-r p:hover {
	box-shadow: 0 1px 0 #b61b17;
	top:1px;
}

/*投稿用公式リンク*/
.rankstlink-r2 p a {
	display: block;
	width: 100%;
	box-sizing:border-box;
	text-align: center;
	padding: 10px;
	background-color: #E53935;
	color: #ffffff;
	text-decoration: none;
	margin-right: auto;
	margin-left: auto;
	border-radius: 5px;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	font-weight:bold;
	box-sizing:border-box;
}

.rankstlink-r2 p {
	width: 90%;
	margin-right: auto;
	margin-left: auto;
	border-radius: 5px;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	box-shadow: 0 2px 0 #b61b17;
	position:relative;
}

.rankstlink-r2 p img{
	display:none;
}

.rankstlink-r2 p br{
	display:none;
}

.rankstlink-r2 p:hover {
	box-shadow: 0 1px 0 #b61b17;
	top:1px;
}

/*アフィリエイトリンクのみ*/

.rankstlink-a p a {
	font-family: Helvetica , "游ゴシック" , "Yu Gothic" , sans-serif;
	display: block;
	width: 100%;
	box-sizing:border-box;
	text-align: center;
	padding: 10px;
	background-color: #E53935;
	color: #ffffff;
	text-decoration: none;
	margin-right: auto;
	margin-left: auto;
	border-radius: 5px;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	font-weight:bold;
}

.rankstlink-a p {
	width: 90%;
	margin-right: auto;
	margin-left: auto;
	border-radius: 5px;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	box-shadow: 0 2px 0 #b61b17;
	position:relative;
}

.rankstlink-a p img{
	display:none;
}

.rankstlink-a p:hover {
	box-shadow: 0 1px 0 #b61b17;
	top:1px;
}


.rankst-box .clearfix.rankst .rankst-l a img, .rankst-box .clearfix.rankst .rankst-l iframe {
	padding:0;
	max-width:100%;
	box-sizing: border-box;
	margin:0 auto;
}

.rankh4:not(.st-css-no), 
.post .rankh4:not(.st-css-no),
#side .rankh4:not(.st-css-no) {
	background-repeat: no-repeat;
	background-position: left center;
	padding-top: 20px;
	padding-right: 20px;
	padding-bottom: 10px;
	padding-left: 80px;
	margin-bottom: 10px;
	border-bottom-width: 1px;
	border-bottom-style: dotted;
	border-bottom-color: #ABA732;
	background-color : transparent ;
	color:#000;
	font-size: 20px;
	line-height: 27px;
			background-image: url(images/oukan.png);
	}


/* 中見出し */
.rankh3:not(.st-css-no) {
	position: relative;
	background: #c5bf3b;
	color: #ffffff!important;
	font-size: 18px;
	line-height: 27px;
	margin-bottom: 20px;
	padding-top: 10px;
	padding-right: 20px;
	padding-bottom: 10px;
	padding-left: 20px;
	border-bottom:none!important;
	text-align:center;
}

.rankh3:not(.st-css-no):after {
	content: '';
	position: absolute;
	border-top: 10px solid #c5bf3b;
	border-right: 10px solid transparent;
	border-left: 10px solid transparent;
	bottom: -10px;
	left: 50%;
	border-radius: 2px;
}

.rankh3:not(.st-css-no):before {
	content: '';
	position: absolute;
	border-top: 10px solid #c5bf3b;
	border-right: 10px solid transparent;
	border-left: 10px solid transparent;
	bottom: -10px;
	left: 50%;
}

.post .rankst-cont h4:not(.st-css-no),
.rankst-cont h4:not(.st-css-no) {
background-color:#FCFC88;
padding:10px;
margin-bottom:10px;
}

/*コンテンツ内の見出し*/
.post .rankst-cont h2:not(.st-css-no),
.post .rankst-cont h3:not(.st-css-no),
.post .rankst-cont h4:not(.st-css-no),
.post .rankst-cont h5:not(.st-css-no) {
	margin-top: 0;
}

/*ランキングナンバー*/

.rankid1 .rankh4:not(.st-css-no), 
.post .rankid1 .rankh4:not(.st-css-no), 
#side .rankid1 .rankh4:not(.st-css-no) {
						background-image: url(images/oukan1.png);
			}

.rankid2 .rankh4:not(.st-css-no), 
.post .rankid2 .rankh4:not(.st-css-no), 
#side .rankid2 .rankh4:not(.st-css-no) {
						background-image: url(images/oukan2.png);
			}

.rankid3 .rankh4:not(.st-css-no), 
.post .rankid3 .rankh4:not(.st-css-no), 
#side .rankid3 .rankh4:not(.st-css-no) {
						background-image: url(images/oukan3.png);
			}

/*サイドバー*/

#side .rankst-l,#side .rankst-r{
	float:none;
	width:100%;
}

#side .rankst-box .clearfix.rankst .rankst-l a img{
	float:none;
	width:100%;
}

#side .rankst-r,#side .rankst-l,#side .rankst-cont{
	margin:0;
}

#side .rankst-ls img {
	max-width: 100% !important;
	margin:0 auto;
}

#side .rankst-ls {
	text-align:center;
}

/*media Queries スマートフォンとタブレットサイズ（959px以下）で適応したいCSS - スマホ・タブレット
---------------------------------------------------------------------------------------------------*/
@media only screen and (max-width: 959px) {


}

/*media Queries タブレットサイズ（600px～959px）のみで適応したいCSS -タブレットのみ
---------------------------------------------------------------------------------------------------*/
@media only screen and (min-width: 600px) and (max-width: 959px) {

	#side .rankst-box .clearfix.rankst .rankst-l a img {
		float: left;
		padding:0;
		margin:0!important;
	}

	#side .rankst-cont {
		margin: 0 0 0 165px;
	}

	#side .rankst-r {
		position:relative;
		z-index:1;
		float: right;
		width: 100%;
		margin: 0 0 0 -150px;
	}

	#side .rankst-l {
		position:relative;
		z-index:2;
		float: left;
		width: 150px;
	}

	#side .rankstlink-l {
		float: left;
		width: 50%;
	}

	#side .rankstlink-r {
		float: right;
		width: 50%;
	}

/*-- ここまで --*/
}

/*media Queries タブレット（600px）以上で適応したいCSS -タブレット・PC
---------------------------------------------------------------------------------------------------*/
@media only screen and (min-width: 600px) {

	.rankst-box .clearfix.rankst .rankst-l a img {
		float: left;
		padding:0;
		margin:0!important;
	}

	.rankst-cont {
		margin: 0 0 0 165px;
	}

	.rankst-r {
		position:relative;
		z-index:1;
		float: right;
		width: 100%;
		margin: 0 0 0 -150px;
	}

	.rankst-l {
		position:relative;
		z-index:2;
		float: left;
		width: 150px;
	}

	
	/*-- ここまで --*/
}

/*media Queries PCサイズ（960px）以上で適応したいCSS - PCのみ
---------------------------------------------------------------------------------------------------*/
@media print, screen and (min-width: 960px) {

	.rankstlink-l {
		float: left;
		width: 50%;
	}

	.rankstlink-r {
		float: right;
		width: 50%;
	}

	#side .rankstlink-l,
	#side .rankstlink-r {
    	float: none;
    	width: 100%;
	}

	/*----------------------------------
	ランク-1カラム
	-----------------------------------*/
	.colum1 .rankst-r {
		float: right;
		width: 100%;
		margin: 0 0 0 -320px;
	}

	.colum1 .rankst-l {
		float: left;
		width: 300px;
	}

	.colum1 .rankst-cont {
		margin: 0 0 0 320px;
	}

	/*投稿用ボタンリンク*/
	.rankstlink-r2 p,.rankstlink-l2 p {
		width: 50%;
	}

	/*-- ここまで --*/
}

/*media Queries スマートフォンのみ（600px）以下
---------------------------------------------------------------------------------------------------*/
@media only screen and (max-width: 599px) {


/*-- ここまで --*/
}